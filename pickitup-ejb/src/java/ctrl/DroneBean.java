/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ctrl;

import java.util.*;
import javax.ejb.*;
import javax.servlet.ServletException;
import model.entity.*;
import model.facade.*;

/**
 * A class that has the responsibility of managing the drones.
 */
@Stateless
public class DroneBean implements DroneBeanLocal {

    @EJB
    private CoverageFacadeLocal coverageFacade;

    @EJB
    private DroneFacadeLocal droneFacade;

    @EJB
    private DeliveryFacadeLocal deliveryFacade;

    /**
     *
     * @param imgURL the URL to the drone's picture
     * @param name the value of name
     * @param weight the value of weight
     * @param speed the value of speed
     * @param autonomy the value of autonomy
     * @param coverage the value of coverage
     * @param maxPayload the value of maxPayload
     * @param maxVolume the value of maxVolume
     * @param flatFee fixed fee to be added to each delivery, in euro
     * @param perKmFee euro paid for each km in each delivery
     * @param owner the drone's owner
     *
     * @return the crated {@link Drone}
     * @throws ServletException
     */
    @Override
    public Drone addDrone(String imgURL, String name, double weight, double speed,
            int autonomy, Coverage coverage, double maxPayload,
            double maxVolume, Double flatFee, Double perKmFee, DroneOwner owner)
            throws ServletException {
        Drone drone = new Drone();
        drone.setImage(imgURL);

        if (name == null || name.equals("")) {
            throw new ServletException("Drone name can't be empty");
        }
        if (speed <= 0.00) {
            throw new ServletException("Speed weight can't be empty neighter negative");
        }
        if (flatFee.compareTo(0.00) <= 0) {
            throw new ServletException("Flat fee can't be empty neighter negative");
        }
        if (weight <= 0.00) {
            throw new ServletException("Drone weight can't be empty neighter negative");
        }
        if (perKmFee <= 0.00) {
            throw new ServletException("Per km fee can't be empty neighter negative");
        }
        if (autonomy <= 0) {
            throw new ServletException("Autonomy can't be empty neighter negative");
        }
        if (coverage.getRadius() <= 0.00) {
            throw new ServletException("Coverage radius can't be empty neighter negative");
        }
        if (maxPayload <= 0.00) {
            throw new ServletException("Maximum payload can't be empty neighter negative");
        }
        if (maxVolume <= 0.00) {
            throw new ServletException("Maximum volume can't be empty neighter negative");
        }

        drone.setName(name);
        drone.setWeight(weight);
        drone.setSpeed(speed);
        drone.setAutonomy(autonomy);
        drone.setCoverage(coverage);
        drone.setMaxPayload(maxPayload);
        drone.setMaxVolume(maxVolume);
        drone.setOwner(owner);
        drone.setPerKmFee(perKmFee);
        drone.setFlatFee(flatFee);

        droneFacade.create(drone);
        return drone;
    }

    /**
     * Deletes a drone
     * @param id The drone to delete
     * @return the success status of the operation
     */
    @Override
    public boolean deleteDrone(long id) {
        Drone drone = droneFacade.find(id);
        if (drone == null) {
            return false;
        }
        // controlla che non ci siano delivery assegnate con stato diverso da proposal e done
        if (!deliveryFacade.deliveryWithThisDrone(drone).stream().noneMatch((del)
                -> (del.getStatus().getValue() != 0
                        && del.getStatus().getValue() != 100))) {
            return false;
        }

        drone.setRemoved(true);
        droneFacade.edit(drone);
        return true;
    }

    /**
     * @param owner a drone owner
     * @return the list of all owner's drones
     */
    @Override
    public List<Drone> getDroneList(DroneOwner owner) {
        return droneFacade.getDronesOwnedBy(owner);
    }

    /**
     * Renames a drone.
     * 
     * @param id the id of the drone to renames
     * @param name the new drone name
     */
    @Override
    public void renameDrone(long id, String name) {
        Drone drone = droneFacade.find(id);
        drone.setName(name);
        droneFacade.edit(drone);
    }

    /**
     * Changes a drone's image
     * @param id The drone to edit
     * @param image The new image URI
     */
    @Override
    public void changeDroneImage(long id, String image) {
        Drone drone = droneFacade.find(id);
        drone.setImage(image);
        droneFacade.edit(drone);
    }

    /**
     * Edits the delivery fees of a drone.
     * 
     * @param id The id of the drone to edit
     * @param flatFee the new flat fee
     * @param perKmFee the new per-kilometer fee
     */
    @Override
    public void setDroneFees(long id, double flatFee, double perKmFee) {
        Drone drone = droneFacade.find(id);
        drone.setFlatFee(flatFee);
        drone.setPerKmFee(perKmFee);
        droneFacade.edit(drone);
    }

    /**
     * @param id A drone id
     * @return The corresponding Drone object
     */
    @Override
    public Drone getDrone(Long id) {
        return droneFacade.find(id);
    }

}
