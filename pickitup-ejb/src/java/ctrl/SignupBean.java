/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ctrl;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.json.gson.GsonFactory;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import model.entity.AppUser;
import model.entity.Client;
import model.entity.DroneOwner;
import model.facade.AppUserFacadeLocal;
import model.facade.ClientFacadeLocal;
import model.facade.DroneOwnerFacadeLocal;
import org.json.JSONObject;

/**
 * A class that has the responsibility of managing the login/signup process,
 * using Google and Facebook APIs.
 */
@Stateless
public class SignupBean implements SignupBeanLocal {

    @EJB
    private DroneOwnerFacadeLocal droneOwnerFacade;

    @EJB
    private ClientFacadeLocal clientFacade;

    @EJB
    private AppUserFacadeLocal appUserFacade;

    static final String GOOGLE_CLIENT_ID = "1082698830205-jpfe92smraars628vf7b0ch7od162scp.apps.googleusercontent.com";
    static final String GOOGLE_APPENT_ID = "728732941646-9qf6tglbtcpfllka8ackt4d79bo3ud8s.apps.googleusercontent.com";

    /**
     * Backend validation of a Google User.
     *
     * @param idToken idToken attribute from the login HttpRequest.
     * @param create
     *
     * @return the corresponding AppUser if validation succeed, null otherwise.
     * @throws java.security.GeneralSecurityException
     * @throws java.io.IOException
     */
    @Override
    public AppUser validateGoogleUser(String idToken, boolean create)
            throws GeneralSecurityException, IOException {
        GoogleIdToken.Payload payload = getGoogleUserPayload(idToken);
        if (payload != null) {
            // Get profile information from payload
            String email = payload.getEmail();

            AppUser tmp = appUserFacade.findUserByEmail(email);
            if (tmp == null && create) {
                tmp = new AppUser();
                tmp.setEmail(email);
                tmp.setSurname((String) payload.get("family_name"));
                tmp.setName((String) payload.get("given_name"));
                tmp.setIMG((String) payload.get("picture"));

                appUserFacade.create(tmp);
            }

            return tmp;
        } else {
            return null;
        }
    }

    /**
     * Verifies a Google user through his token, sent by the client application
     *
     * @param token the user's token
     * @return a Payload object containing the user information
     *
     * @throws GeneralSecurityException
     * @throws IOException
     */
    private GoogleIdToken.Payload getGoogleUserPayload(String token)
            throws GeneralSecurityException, IOException {
        ArrayList<String> tokens = new ArrayList<>();
        tokens.add(GOOGLE_CLIENT_ID);
        tokens.add(GOOGLE_APPENT_ID);

        GoogleIdTokenVerifier verifier = new GoogleIdTokenVerifier.Builder(
                GoogleNetHttpTransport.newTrustedTransport(), GsonFactory.getDefaultInstance())
                .setAudience(tokens)
                .build();

        // (Receive idTokenString by HTTPS POST)
        GoogleIdToken gIdToken = verifier.verify(token);
        return gIdToken == null ? null : gIdToken.getPayload();
    }

    /**
     * Backend validation of a Facebook User.
     *
     * @param token Facebook token
     * @param create
     * @return the corresponding AppUser if validation succeed, null otherwise.
     * @throws java.net.MalformedURLException
     */
    @Override
    public AppUser validateFacebookUser(String token, boolean create) throws MalformedURLException, IOException {
        JSONObject userJson = getFacebookUserJson(token);

        AppUser tmp = appUserFacade.findUserByEmail(userJson.getString("email"));
        if (tmp == null && create) {
            tmp = new AppUser();
            tmp.setEmail(userJson.getString("email"));
            tmp.setName(userJson.getString("name"));
            tmp.setIMG(userJson.getJSONObject("picture").getJSONObject("data").getString("url"));

            appUserFacade.create(tmp);
        }

        return tmp;
    }

    /**
     * Verifies a Facebook user through his token, sent by the client
     * application .
     *
     * @param token The user token
     * @return a JSONObject containing the user information
     *
     * @throws MalformedURLException
     * @throws IOException
     */
    private JSONObject getFacebookUserJson(String token)
            throws MalformedURLException, IOException {
        URL validationUrl = new URL("https://graph.facebook.com/me?fields=id,name,email,picture&access_token=" + token);
        HttpURLConnection conn = (HttpURLConnection) validationUrl.openConnection();

        StringBuilder b;
        try (BufferedReader in = new BufferedReader(
                new InputStreamReader(conn.getInputStream()))) {
            String inputLine;
            b = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                b.append(inputLine).append("\n");
            }
        }
        String graph = b.toString();
        JSONObject jsonOut = new JSONObject(graph);
        return jsonOut.has("error") ? null : jsonOut;
    }

    /**
     * Changes the role of an user that has just been created.
     *
     * @param id The id of the user to modify
     * @param role The role this user will have (Client or DroneOwner)
     * @return the modified AppUser object, instance of either Client or
     * DroneOwner
     */
    @Override
    public AppUser changeUserPrivilege(Long id, String role) {

        AppUser tmp, user = appUserFacade.find(id);

        if (user == null || !user.getClass().equals(AppUser.class)) {
            throw new IllegalStateException("Inexisting or already set user");
        }

        appUserFacade.remove(user);
        switch (role) {
            case "DroneOwner":
                tmp = new DroneOwner();
                tmp.copyFromUser(user);
                droneOwnerFacade.create((DroneOwner) tmp);
                break;
            case "Client":
                tmp = new Client();
                tmp.copyFromUser(user);
                clientFacade.create((Client) tmp);
                break;
            default:
                throw new RuntimeException(role + " is not a valid class name!");
        }
        return tmp;
    }

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
}
