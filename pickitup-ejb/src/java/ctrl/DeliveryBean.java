/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ctrl;

import java.util.List;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import model.entity.AppUser;
import model.entity.Client;
import model.entity.Coverage;
import model.entity.Delivery;
import model.entity.Drone;
import model.entity.DroneOwner;
import model.entity.LatLng;
import model.entity.Parcel;
import model.facade.DeliveryFacadeLocal;
import model.facade.DroneFacadeLocal;
import model.facade.DroneOwnerFacadeLocal;
import model.facade.LatLngFacadeLocal;
import model.facade.ParcelFacadeLocal;

/**
 * A class that has the responsibility of managing the deliveries.
 */
@Stateless
public class DeliveryBean implements DeliveryBeanLocal {

    @EJB
    private LatLngFacadeLocal latLngFacade;

    @EJB
    private ParcelFacadeLocal parcelFacade;

    @EJB
    private DeliveryFacadeLocal deliveryFacade;

    @EJB
    private DroneOwnerFacadeLocal droneOwnerFacade;

    @EJB
    private DroneFacadeLocal droneFacade;

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    /**
     * @param user an application user, that can be a Client or a DroneOwner
     * @return the list of all deliveries in which user is someway concerned
     */
    @Override
    public List<Delivery> getUserDeliveryList(AppUser user) {
        List<Delivery> deliveries = deliveryFacade.findDroneOwnerDeliveries(user);
        deliveries.addAll(deliveryFacade.findClientDeliveries(user));
        return deliveries;
    }

    /**
     * @param client The client that is performing the research
     * @param parcelId The parcel the client wants to send
     * @param destLat the destination latitude
     * @param destLng the destination longitude
     * @return a list of all the Delivery proposals that can satisfy the
     * client's desire
     */
    @Override
    public List<Delivery> searchDeliveries(Client client, long parcelId,
            double destLat, double destLng) {
        Parcel parcel = parcelFacade.find(parcelId);
        LatLng destination = new LatLng();
        destination.setLatitude(destLat);
        destination.setLongitude(destLng);
        latLngFacade.create(destination);

        // Filter according to the drone's max payload and volume
        List<Drone> drones = droneFacade.findDronesWithMax(
                parcel.getVolume(), parcel.getWeight());
        // Filter according to the drone's coverage and autonomy
        drones.removeIf(drone -> {
            Coverage coverage = drone.getCoverage();
            return !(drone.hasEnoughAutonomy(destination)
                    && coverage.contains(client.getPosition()) && coverage.contains(destination));
        });
        return drones.stream()
                .map(drone -> {
                    Delivery d = new Delivery();
                    d.setClient(client);
                    d.setParcel(parcel);
                    d.setDrone(drone);
                    d.setDestination(destination);
                    d.setStatus(Delivery.Status.PROPOSAL);
                    deliveryFacade.create(d);
                    return d;
                })
                .sorted()
                .collect(Collectors.toList());
    }

    @Override
    public List<DroneOwner> getSurroundingDroneOwners(LatLng positionClient) {
        return droneOwnerFacade.getSurroundingDroneOwners(positionClient);
    }

    /**
     * Updates the status of a delivery.
     *
     * @param delivery the delivery to update
     * @param status the new delivery status
     */
    @Override
    public void updateStatus(Delivery delivery, Delivery.Status status) {
        delivery.setStatus(status);
        deliveryFacade.edit(delivery);
    }

    /**
     * Cleans all the unuseful delivery proposals.
     */
    @Override
    public void cleanProposals() {
        deliveryFacade.cleanProposals();
    }

    /**
     * @param id the id of a delivery
     * @return the corresponding Delivery object
     */
    @Override
    public Delivery getDelivery(long id) {
        return deliveryFacade.find(id);
    }
}
