/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ctrl;

import java.util.List;
import javax.ejb.Local;
import model.entity.AppUser;
import model.entity.Client;
import model.entity.Delivery;
import model.entity.DroneOwner;
import model.entity.LatLng;

/**
 *
 * @author marco
 */
@Local
public interface DeliveryBeanLocal {

    List<Delivery> getUserDeliveryList(AppUser user);

    List<Delivery> searchDeliveries(Client client, long parcelId, double destLat, double destLng);

    List<DroneOwner> getSurroundingDroneOwners(LatLng positionClient);

    void updateStatus(Delivery parcelId, Delivery.Status status);

    void cleanProposals();

    Delivery getDelivery(long id);

}
