package ctrl;

public class Validator {

    public static boolean validateName(String nome) {
        return nome == null
                ? false : nome.matches("^(([a-zA-Z]{2,}\\s?){1,3})$");
    }

    public static boolean validateSurname(String cognome) {
        return cognome == null
                ? false : cognome.matches("^((([a-zA-Z]+'[a-zA-Z]+)|([a-zA-Z]+)\\s?){1,3})$");
    }

    public static boolean validateEmail(String mail) {
        return mail == null
                ? false : mail.matches("^[a-zA-Z0-9]+(([\\.|_]?([a-zA-Z0-9]+)))*@[a-zA-Z]+([\\.|_]?([a-zA-Z0-9]+))*.[a-zA-Z]{2,3}$");
    }

    public static boolean validatePhoneNumber(String telefono) {
        return telefono == null
                ? false : telefono.matches("^(\\+?[0-9]{4,14})$");
    }

    public static boolean validatePrice(String prezzo) {
        return prezzo == null
                ? false : prezzo.matches("^([0-9]{1,2}((,|.)[0-9]{1,2})?)$");
    }

    public static boolean validateSex(String sesso) {
        return sesso == null
                ? false : sesso.matches("^(M|m|F|f)$");
    }

    public static boolean validateInteger(String integerNum) {
        return integerNum == null
                ? false : integerNum.matches("^\\d+$");
    }

    public static boolean validateDouble(String doubleNum) {
        return doubleNum == null
                ? false : doubleNum.matches("^[0-9]+[.]?[0-9]*$");
    }

    /**
     * Valida il formato yyyy-MM-dd
     * @param date
     * @return 
     */
    public static boolean validateDate(String date) {
        return date == null
                ? false : date.matches("^(19|20)\\d\\d[-](0[1-9]|1[012])[-](0[1-9]|[12][0-9]|3[01])$");
    }

    /**
     * Valida il formato yyyy-MM-dd hh:mm:ss
     * @param datetime
     * @return 
     */
    public static boolean validateDatetime(String datetime) {
        return datetime == null
                ? false : datetime.matches("^(19|20)\\d\\d[-](0[1-9]|1[012])[-](0[1-9]|[12][0-9]|3[01])\\s([0-1]?[0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]$");
    }

    /**
     * Valida il formato yyyy-MM-dd hh:mm:ss.SSS
     * @param datetime
     * @return 
     */
    public static boolean validateDatetimeSQL(String datetime) {
        return datetime == null
                ? false : datetime.matches("^(19|20)\\d\\d[-](0[1-9]|1[012])[-](0[1-9]|[12][0-9]|3[01])\\s([0-1]?[0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]\\.[0-9]{3}$");
    }

    /**
     * Valida il formato yyyy-MM-ddThh:mm
     * @param datetime
     * @return 
     */
    public static boolean validateDatetimeHTML(String datetime) {
        return datetime == null
                ? false : datetime.matches("^(19|20)\\d\\d[-](0[1-9]|1[012])[-](0[1-9]|[12][0-9]|3[01])T([0-1]?[0-9]|2[0-3]):[0-5][0-9]$");
    }

    /**
     * Valida la password
     * @param password
     * @return 
     */
    public static boolean validatePassword(String password) {
        return password == null
                ? false : password.matches("^(\\S){1,30}$");

    }

    /**
     * Valida l'indirizzo
     * @param address
     * @return 
     */
    public static boolean validateAddress(String address) {
        return address != null;

    }

}
