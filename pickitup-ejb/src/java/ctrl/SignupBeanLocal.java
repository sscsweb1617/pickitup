/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ctrl;

import java.io.IOException;
import java.net.MalformedURLException;
import java.security.GeneralSecurityException;
import javax.ejb.Local;
import model.entity.AppUser;

/**
 *
 * @author marco
 */
@Local
public interface SignupBeanLocal {

    AppUser changeUserPrivilege(Long id, String role);

    AppUser validateGoogleUser(String data, boolean create) throws GeneralSecurityException, IOException;

    AppUser validateFacebookUser(String data, boolean create) throws MalformedURLException, IOException;

}
