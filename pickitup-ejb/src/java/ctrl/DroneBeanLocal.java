/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ctrl;

import java.util.List;
import javax.ejb.*;
import javax.servlet.ServletException;
import model.entity.*;

/**
 *
 * @author andrea
 */
@Local
public interface DroneBeanLocal {

    Drone addDrone(String imgURL, String name, double weight, double speed, int autonomy, Coverage coverage, double maxPayload, double maxVolume, Double flatFee, Double perKmFee, DroneOwner owner) throws ServletException;

    boolean deleteDrone(long id);

    List<Drone> getDroneList(DroneOwner owner);
    
    Drone getDrone(Long id);

    void renameDrone(long id, String name);

    void changeDroneImage(long id, String image);

    void setDroneFees(long id, double flatFee, double perKmFee);
}
