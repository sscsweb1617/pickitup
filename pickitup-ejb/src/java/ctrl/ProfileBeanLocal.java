/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ctrl;

import javax.ejb.Local;
import model.entity.AppUser;

/**
 *
 * @author marco
 */
@Local
public interface ProfileBeanLocal {

    boolean setPaypal(Long id, String newPaypalMail);

    boolean setName(Long id, String name);

    boolean setSurname(Long id, String surname);

    boolean setPhoneNumber(Long id, String newNumber);

    void setPosition(AppUser user, double latitude, double longitude);

}
