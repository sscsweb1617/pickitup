/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ctrl;

import java.util.List;
import javax.ejb.Local;
import model.entity.Client;
import model.entity.Parcel;

/**
 *
 * @author marco
 */
@Local
public interface ParcelBeanLocal {

    void create(Client client, String name, double estimatedValue, double volume, double weight);

    List<Parcel> getUserParcelList(Client client);

    void deleteParcel(Long id);

    boolean controlStatusParcel(Long idParcel);

    Parcel getParcel(Long idParcel);
    
}
