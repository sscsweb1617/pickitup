/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ctrl;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import model.entity.AppUser;
import model.entity.LatLng;
import model.facade.AppUserFacadeLocal;
import model.facade.LatLngFacadeLocal;

/**
 * A class that has the responsibility of managing the profile information.
 */
@Stateless
public class ProfileBean implements ProfileBeanLocal {

    @EJB
    private LatLngFacadeLocal latLngFacade;

    @EJB
    private AppUserFacadeLocal appUserFacade;

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    
    /**
     * Changes the user's PayPal address.
     * 
     * @param id the id of the user to edit
     * @param newPaypalMail the new user's PayPal address
     * @return the success status of the operation
     */
@Override
    public boolean setPaypal(Long id, String newPaypalMail) {
        if (!Validator.validateEmail(newPaypalMail)) {
            throw new IllegalArgumentException(newPaypalMail + " is not a valid email address!");
        }
        AppUser user = appUserFacade.find(id);
        if (user == null) {
            return false;
        }
        user.setPaypal(newPaypalMail);
        appUserFacade.edit(user);
        return true;
    }

    /**
     * Changes the user's name.
     * 
     * @param id the id of the user to edit
     * @param name the new user's name
     * @return the success status of the operation
     */
    @Override
    public boolean setName(Long id, String name) {
        if (!Validator.validateName(name)) {
            throw new IllegalArgumentException(name + " is not a valid name!");
        }
        AppUser user = appUserFacade.find(id);
        if (user == null) {
            return false;
        }
        user.setName(name);
        appUserFacade.edit(user);
        return true;
    }

    /**
     * Changes the user's surname.
     * 
     * @param id the id of the user to edit
     * @param surname the new user's surname
     * @return the success status of the operation
     */
    @Override
    public boolean setSurname(Long id, String surname) {
        if (!Validator.validateSurname(surname)) {
            throw new IllegalArgumentException(surname + " is not a valid surname!");
        }
        AppUser user = appUserFacade.find(id);
        if (user == null) {
            return false;
        }
        user.setSurname(surname);
        appUserFacade.edit(user);
        return true;
    }

    /**
     * Changes the user's phone number.
     * 
     * @param id the id of the user to edit
     * @param newNumber the new user's phone number
     * @return the success status of the operation
     */
@Override
    public boolean setPhoneNumber(Long id, String newNumber) {
        if (!Validator.validatePhoneNumber(newNumber)) {
            throw new IllegalArgumentException(newNumber + " is not a valid phone number!");
        }
        AppUser user = appUserFacade.find(id);
        if (user == null) {
            return false;
        }
        user.setPhoneNumber(newNumber);
        appUserFacade.edit(user);
        return true;
    }

    /**
     * Changes the user's position.
     * 
     * @param user the user to edit
     * @param latitude the user's new latitude
     * @param longitude the user's new longitude
     */
    @Override
    public void setPosition(AppUser user, double latitude, double longitude) {
        LatLng position = new LatLng();
        position.setLatitude(latitude);
        position.setLongitude(longitude);
        latLngFacade.create(position);

        user.setPosition(position);
        appUserFacade.edit(user);
    }
}
