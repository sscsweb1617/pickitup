/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ctrl;

import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import model.entity.Client;
import model.entity.Delivery;
import model.entity.Delivery.Status;
import model.entity.Parcel;
import model.facade.ClientFacadeLocal;
import model.facade.DeliveryFacadeLocal;
import model.facade.ParcelFacadeLocal;

/**
 * A class that has the responsibility of managing the parcels.
 */
@Stateless
public class ParcelBean implements ParcelBeanLocal {

    @EJB
    private DeliveryFacadeLocal deliveryFacade;

    @EJB
    private ClientFacadeLocal clientFacade;

    @EJB
    private ParcelFacadeLocal parcelFacade;

    /**
     * Creates a new Parcel
     *
     * @param client the new parcel's client
     * @param name the new parcel's name
     * @param estimatedValue the new parcel's estimated value
     * @param volume the new parcel's volume
     * @param weight the new parcel's weight
     */
    @Override
    public void create(Client client, String name, double estimatedValue,
            double volume, double weight) {
        Parcel parcel = new Parcel();
        parcel.setName(name);
        parcel.setEstimatedValue(estimatedValue);
        parcel.setVolume(volume);
        parcel.setWeight(weight);
        parcel.setOwner(client);
        parcel.setRemoved(false);
        parcelFacade.create(parcel);
    }

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    /**
     * Gets a list of all the parcels owned by a certain user
     *
     * @param client The user
     * @return a list of all the parcels owned by client
     */
    @Override
    public List<Parcel> getUserParcelList(Client client) {
        return parcelFacade.getUserParcelList(client);
    }

    /**
     * Finds a parcel by its id.
     *
     * @param idParcel The id of the parcel
     * @return The corresponding Parcel object
     */
    @Override
    public Parcel getParcel(Long idParcel) {
        return parcelFacade.find(idParcel);
    }

    /**
     * Deletes a parcel.
     *
     * @param id The id of the parcel to delete
     */
    @Override
    public void deleteParcel(Long id) {
        Parcel parcel = new Parcel();
        parcel.setId(id);

        parcel = parcelFacade.find(id);
        parcel.setRemoved(true);
        parcelFacade.edit(parcel);
    }

    /**
     * Checks if a parcel can be safely removed.
     *
     * @param idParcel the pacrel to check
     * @return true if the parcel can be safely removed, false otherwise
     */
    @Override
    public boolean controlStatusParcel(Long idParcel) {
        Parcel parcel = new Parcel();
        parcel.setId(idParcel);

        deliveryFacade.cleanProposals();
        List<Delivery> d = deliveryFacade.deliveryWithThisParcel(parcel);

        if (d.isEmpty()) {
            return true;
        }

        Status status = d.get(0).getStatus();
        return status == Status.BOOKED;
    }
}
