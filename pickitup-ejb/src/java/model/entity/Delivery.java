/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.entity;

import java.io.*;
import javax.persistence.*;

/**
 * Defines information about a delivery.
 *
 * @author marco
 */
@Entity
public class Delivery implements Serializable, Comparable<Delivery> {

    public final static double WALKING_SPEED = 5;

    /**
     * Status of a delivery.
     *
     */
    Status status;

    @Override
    public int compareTo(Delivery d) {
        return Double.compare(this.getCost(), d.getCost());
    }

    /**
     * The status of a delivery
     */
    public enum Status {
        PROPOSAL(0), BOOKED(10), ACCEPTED(35), SENT(70), DONE(100);
        private final int value;

        Status(final int newValue) {
            value = newValue;
        }
        
        /**
         * @return the "value" of this status, interpretable as a percentage.
         */
        public int getValue() {
            return value;
        }

        @Override
        public String toString() {
            String s = super.toString().toLowerCase();
            return Character.toUpperCase(s.charAt(0)) + s.substring(1);
        }

        /**
         * @return A human-readable description of the actual status
         */
        public String getDescription() {
            switch (this) {
                case PROPOSAL:
                    return "Click on the \'Start delivery\' button to book this "
                            + "proposal. You will be redirected to the PayPal "
                            + "website.";
                case BOOKED:
                    return "This delivery has been booked. The parcel must be brought to the drone owner!";
                case ACCEPTED:
                    return "The drone owner will send your parcel soon.";
                case SENT:
                    return "Your parcel is flying now! :D";
                case DONE:
                    return "This delivery has been completed.";
            }
            throw new IllegalStateException("Unknown status!");
        }
    }

    private static final long serialVersionUID = 1L;
    /**
     * Delivery primary key.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    /**
     * The user needs to send a parcel.
     */
    @ManyToOne
    Client client;

    /**
     * Destination of a delivery, expressed by its latitude and longitude.
     */
    @OneToOne
    LatLng destination;

    /**
     * Drone used to send parcel.
     */
    @ManyToOne
    Drone drone;

    /**
     * A box next to be sent.
     */
    @ManyToOne
    Parcel parcel;

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public double getCost() {
        double distDrone = drone.owner.position.distanceFrom(destination);
        return drone.flatFee + (distDrone * drone.perKmFee);
    }

    public LatLng getDestination() {
        return destination;
    }

    public void setDestination(LatLng destination) {
        this.destination = destination;
    }

    public Drone getDrone() {
        return drone;
    }

    public void setDrone(Drone drone) {
        this.drone = drone;
    }

    public int getEstimatedTime() {
        double distWalking = client.position.distanceFrom(drone.owner.position);
        double distDrone = drone.owner.position.distanceFrom(destination);
        return Math.round(60 * (float) ((distWalking / WALKING_SPEED) + (distDrone / drone.speed)));
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LatLng getInitialPosition() {
        return client.position;
    }

    public Parcel getParcel() {
        return parcel;
    }

    public void setParcel(Parcel parcel) {
        this.parcel = parcel;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Delivery)) {
            return false;
        }
        Delivery other = (Delivery) object;
        return !((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)));
    }

    @Override
    public String toString() {
        return parcel + " --" + drone + "-> " + destination;
    }

    public LatLng getDronePosition() {
        return drone.owner.position;
    }
}
