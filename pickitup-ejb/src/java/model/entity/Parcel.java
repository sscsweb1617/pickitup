/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.entity;

import java.io.*;
import javax.persistence.*;

/**
 * A box next to be sent.
 * @author marco
 */
@Entity
public class Parcel implements Serializable {

    private static final long serialVersionUID = 1L;
    
    /**
     * Database primary key
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    private String name;

    /**
     * Parcel estimated value in euro
     */
    double estimatedValue;

    /**
     * Parcel volume in centimeters cube
     */
    double volume;

    /**
     * Parcel weight in kg
     */
    double weight;
    
    /**
     * It shows if parcel was removed by client
     */
    boolean removed;

    /**
     * Get the value of removed
     *
     * @return the value of removed
     */
    public boolean isRemoved() {
        return removed;
    }

    /**
     * Set the value of removed
     *
     * @param removed new value of removed
     */
    public void setRemoved(boolean removed) {
        this.removed = removed;
    }

    
    @ManyToOne
    Client owner;

    /**
     * Get the value of owner
     *
     * @return the value of owner
     */
    public Client getOwner() {
        return owner;
    }

    /**
     * Set the value of owner
     *
     * @param owner new value of owner
     */
    public void setOwner(Client owner) {
        this.owner = owner;
    }

    /**
     * Get the value of name
     *
     * @return the value of name
     */
    public String getName() {
        return name;
    }

    /**
     * Set the value of name
     *
     * @param name new value of name
     */
    public void setName(String name) {
        this.name = name;
    }

    public double getEstimatedValue() {
        return estimatedValue;
    }

    public void setEstimatedValue(double estimatedValue) {
        this.estimatedValue = estimatedValue;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public double getVolume() {
        return volume;
    }

    public void setVolume(double volume) {
        this.volume = volume;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if(!(object instanceof Parcel)) {
            return false;
        }
        Parcel other = (Parcel) object;
        return !((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)));
    }

    @Override
    public String toString() {
        return name;
    }

}
