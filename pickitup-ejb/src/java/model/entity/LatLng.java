/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.entity;

import java.io.*;
import javax.persistence.*;
import org.json.JSONObject;

/**
 * A position in the world, expressed by its latitude and longitude.
 *
 * @author marco
 */
@Entity
public class LatLng implements Serializable {

    private static final long serialVersionUID = 1L;

    public static LatLng random() {
        double lat = 45.0092 + (45.117332 - 45.0092) * Math.random();
        double lng = 7.698594 + (7.591389 - 7.698594) * Math.random();

        LatLng pos = new LatLng();
        pos.setLatitude(lat);
        pos.setLongitude(lng);
        
        return pos;
    }

    /**
     * Database primary key
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    /**
     * North-south coordinate, in the interval [-90, +90]
     */
    double latitude;

    /**
     * West-east coordinate, in the interval [-180, +180]
     */
    double longitude;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof LatLng)) {
            return false;
        }
        LatLng other = (LatLng) object;
        return !((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)));
    }

    @Override
    public String toString() {
        return String.format("%.4f lat; %.4f lng", latitude, longitude);
    }

    /**
     * @param position
     * @return The distance in kilometers
     */
    double distanceFrom(LatLng position) {
        final int R = 6371; // Radius of the earth

        double latDistance = Math.toRadians(position.latitude - latitude);
        double lonDistance = Math.toRadians(position.longitude - longitude);
        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(Math.toRadians(latitude)) * Math.cos(Math.toRadians(position.latitude))
                * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        return R * c;
    }

    public JSONObject toJson() {
        JSONObject json = new JSONObject();
        json.put("lat", latitude);
        json.put("lng", longitude);
        return json;
    }
}
