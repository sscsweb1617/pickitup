/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.entity;

import java.io.*;
import javax.persistence.*;

/**
 * Defines information about drone.
 *
 * @author marco
 */
@Entity
public class Drone implements Serializable {

    /**
     * Owner's drone.
     */
    @ManyToOne
    DroneOwner owner;

    public DroneOwner getOwner() {
        return owner;
    }

    private static final long serialVersionUID = 1L;

    /**
     * Drone primary key.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    /**
     * The drone's autonomy, in minutes
     */
    int autonomy;

    /**
     * The drone's coverage area
     */
    @OneToOne
    Coverage coverage;

    /**
     * Flat fee for using this drone
     */
    double flatFee;

    /**
     * The drone image's URI
     */
    String image = "http://placehold.it/400/400/cats";

    /**
     * Maximum weight of a parcel being delivered by this drone
     */
    double maxPayload;

    /**
     * Maximum volume of a parcel being delivered by this drone
     */
    double maxVolume;

    /**
     * The drone's name
     */
    String name;

    /**
     * Per-km fee for using this drone
     */
    double perKmFee;

    /**
     * Drone speed, in km/h
     */
    double speed;

    /**
     * Drone weight, in kilograms
     */
    double weight;

    /**
     * Drone removal is "soft", so that the DBMS won't get any inconsistency
     */
    boolean removed;

    public int getAutonomy() {
        return autonomy;
    }

    public void setAutonomy(int autonomy) {
        this.autonomy = autonomy;
    }

    public Coverage getCoverage() {
        return coverage;
    }

    public void setCoverage(Coverage coverage) {
        this.coverage = coverage;
    }

    public double getFlatFee() {
        return flatFee;
    }

    public void setFlatFee(double flatFee) {
        this.flatFee = flatFee;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public double getMaxPayload() {
        return maxPayload;
    }

    public void setMaxPayload(double maxPayload) {
        this.maxPayload = maxPayload;
    }

    public double getMaxVolume() {
        return maxVolume;
    }

    public void setMaxVolume(double maxVolume) {
        this.maxVolume = maxVolume;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPerKmFee() {
        return perKmFee;
    }

    public void setPerKmFee(double perKmFee) {
        this.perKmFee = perKmFee;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public void setOwner(DroneOwner owner) {
        this.owner = owner;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public void setRemoved(boolean removed) {
        this.removed = removed;
    }

    public boolean getRemoved() {
        return removed;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Drone)) {
            return false;
        }
        Drone other = (Drone) object;
        return !((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)));
    }

    @Override
    public String toString() {
        return name + " [" + id + "]";
    }

    /**
     *
     * @param destination
     * @return Time estimation in minutes
     */
    public int estimateTime(LatLng destination) {
        double distance = owner.getPosition().distanceFrom(destination);
        return (int) Math.round(distance / speed * 60);
    }

    public double getCost(LatLng destination) {
        double distance = owner.getPosition().distanceFrom(destination);
        return flatFee + distance * perKmFee;
    }

    public boolean hasEnoughAutonomy(LatLng destination) {
        return autonomy >= estimateTime(destination);
    }
}
