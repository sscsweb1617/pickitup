/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.entity;

import java.awt.geom.Path2D;
import java.io.*;
import java.util.*;
import javax.persistence.*;

/**
 * The coverage area of a drone.
 * It can be an polygon (if center == null) or a center-radius couple
 * (otherwise).
 */
@Entity
public class Coverage implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Coverage primary key.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    /**
     * A position in the world, expressed by its latitude and longitude.
     */
    @OneToOne
    LatLng center;

    /**
     * An area in the world, expressed by a list consisting of latitude and
     * longitude.
     */
    @OneToMany
    ArrayList<LatLng> polygon;

    /**
     * Radius of an area.
     */
    double radius;

    public LatLng getCenter() {
        return center;
    }

    public void setCenter(LatLng center) {
        this.center = center;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ArrayList<LatLng> getPolygon() {
        return polygon;
    }

    public void setPolygon(ArrayList<LatLng> polygon) {
        this.polygon = polygon;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Coverage)) {
            return false;
        }
        Coverage other = (Coverage) object;
        return !((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)));
    }

    @Override
    public String toString() {
        return "model.Coverage[ id=" + id + " ]";
    }

    public boolean contains(LatLng position) {
        if (center != null) {
            return center.distanceFrom(position) <= radius;
        } else {
            Path2D path = new Path2D.Double();
            path.moveTo(polygon.get(0).latitude, polygon.get(0).longitude);
            for (int i = 1; i < polygon.size(); i++) {
                path.lineTo(polygon.get(i).latitude, polygon.get(i).longitude);
            }
            path.closePath();
            return path.contains(position.latitude, position.longitude);
        }
    }
}
