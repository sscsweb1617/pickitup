/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.entity;

import java.io.*;
import java.util.*;
import javax.persistence.*;

/**
 * A user owning a single drone or a list of them.
 *
 * @author marco
 */
@Entity
public class DroneOwner extends AppUser implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * The list of drones owned
     */
    @OneToMany(mappedBy = "owner", fetch = FetchType.EAGER)
    List<Drone> droneList;

    public List<Drone> getDroneList() {
        return droneList;
    }

    public void setDroneList(List<Drone> droneList) {
        this.droneList = droneList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (email != null ? email.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof DroneOwner)) {
            return false;
        }
        DroneOwner other = (DroneOwner) object;
        return !((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)));
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }
}
