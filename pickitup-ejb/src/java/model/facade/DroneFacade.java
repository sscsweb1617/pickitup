/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.facade;

import java.util.*;
import javax.ejb.*;
import javax.persistence.*;
import javax.persistence.criteria.*;
import model.entity.*;

/**
 *
 * @author marco
 */
@Stateless
public class DroneFacade extends AbstractFacade<Drone> implements DroneFacadeLocal {

    @PersistenceContext(unitName = "pickitup-PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public DroneFacade() {
        super(Drone.class);
    }

    @Override
    public List<Drone> getDronesOwnedBy(DroneOwner owner) {
        // Una simil-factory
        CriteriaBuilder cb = em.getCriteriaBuilder();
        // SELECT
        CriteriaQuery<Drone> cq = cb.createQuery(Drone.class);
        // FROM
        Root<Drone> root = cq.from(Drone.class);
        // WHERE
        cq.where(cb.and(cb.equal(root.get(Drone_.owner), owner),cb.equal(root.get(Drone_.removed),false)));
        // Esecuzione
        return em.createQuery(cq).getResultList();
    }

    @Override
    public List<Drone> findDronesWithMax(double volume, double weight) {
        // Una simil-factory
        CriteriaBuilder cb = em.getCriteriaBuilder();
        // SELECT
        CriteriaQuery<Drone> cq = cb.createQuery(Drone.class);
        // FROM
        Root<Drone> root = cq.from(Drone.class);
        // WHERE
        cq.where(cb.greaterThanOrEqualTo(root.get(Drone_.maxVolume), volume),
                cb.greaterThanOrEqualTo(root.get(Drone_.maxPayload), weight));
        // Esecuzione
        return em.createQuery(cq).getResultList();
    }

}
