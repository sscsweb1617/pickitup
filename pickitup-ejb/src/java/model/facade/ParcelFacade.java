/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.facade;

import java.util.ArrayList;
import java.util.List;
import model.entity.Parcel;
import javax.ejb.*;
import javax.persistence.*;
import model.*;
import model.entity.Client;

/**
 *
 * @author marco
 */
@Stateless
public class ParcelFacade extends AbstractFacade<Parcel> implements ParcelFacadeLocal {

    @PersistenceContext(unitName = "pickitup-PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ParcelFacade() {
        super(Parcel.class);
    }
    
    @Override
    public List<Parcel> getUserParcelList(Client client) {
        return em.createQuery("SELECT p FROM Parcel p WHERE p.owner = :id and p.removed = 0")
                .setParameter("id", client)
                .getResultList();
    }
    
}
