/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.facade;

import model.entity.LatLng;
import java.util.*;
import javax.ejb.*;
import model.*;

/**
 *
 * @author marco
 */
@Local
public interface LatLngFacadeLocal {

    void create(LatLng latLng);

    void edit(LatLng latLng);

    void remove(LatLng latLng);

    LatLng find(Object id);

    List<LatLng> findAll();

    List<LatLng> findRange(int[] range);

    int count();
    
}
