/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.facade;

import java.util.ArrayList;
import java.util.List;
import model.entity.Delivery;
import javax.ejb.*;
import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import model.entity.*;

/**
 *
 * @author marco
 */
@Stateless
public class DeliveryFacade extends AbstractFacade<Delivery> implements DeliveryFacadeLocal {

    @EJB
    private ClientFacadeLocal clientFacade;

    @PersistenceContext(unitName = "pickitup-PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public DeliveryFacade() {
        super(Delivery.class);
    }

    @Override
    public List<Delivery> findDroneOwnerDeliveries(AppUser user) {
        String query = "SELECT del.* FROM AppUser a "
                + "JOIN Drone dr on a.id = dr.owner_id "
                + "JOIN Delivery del ON dr.id = del.drone_id "
                + "WHERE del.status <> 0 AND a.id = ?";
        Query q = em.createNativeQuery(query, Delivery.class);
        q.setParameter(1, user.getId());
        return q.getResultList();
    }

    @Override
    public void cleanProposals() {
        String deleteQuery = "DELETE FROM Delivery WHERE STATUS=0";
        em.createNativeQuery(deleteQuery).executeUpdate();
    }

    @Override
    public List<Delivery> findClientDeliveries(AppUser user) {
        if (user instanceof DroneOwner) {
            return new ArrayList<>();
        }
        // Una simil-factory
        CriteriaBuilder cb = em.getCriteriaBuilder();
        // SELECT
        CriteriaQuery<Delivery> cq = cb.createQuery(Delivery.class);
        // FROM
        Root<Delivery> root = cq.from(Delivery.class);
        // WHERE
        cq.where(cb.and(
                cb.notEqual(root.get(Delivery_.status), Delivery.Status.PROPOSAL),
                cb.equal(root.get(Delivery_.client), user)));
        // Esecuzione
        return em.createQuery(cq).getResultList();
    }

    @Override
    public List<Delivery> deliveryWithThisParcel(Parcel idParcel) {
        return em.createQuery("SELECT d FROM Delivery d WHERE d.parcel = :id", Delivery.class)
                .setParameter("id", idParcel)
                .getResultList();    
    }
    
    @Override
    public List<Delivery> deliveryWithThisDrone(Drone drone) {
        return em.createQuery("SELECT d FROM Delivery d WHERE d.drone = :id", Delivery.class)
                .setParameter("id", drone)
                .getResultList();    
    }

}
