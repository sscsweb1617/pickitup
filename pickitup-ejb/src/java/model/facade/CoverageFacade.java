/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.facade;

import model.entity.Coverage;
import javax.ejb.*;
import javax.persistence.*;
import model.*;

/**
 *
 * @author marco
 */
@Stateless
public class CoverageFacade extends AbstractFacade<Coverage> implements CoverageFacadeLocal {

    @PersistenceContext(unitName = "pickitup-PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CoverageFacade() {
        super(Coverage.class);
    }
    
}
