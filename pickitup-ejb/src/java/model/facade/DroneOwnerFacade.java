/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.facade;

import java.util.ArrayList;
import java.util.List;
import model.entity.DroneOwner;
import javax.ejb.*;
import javax.persistence.*;
import model.*;
import model.entity.LatLng;

/**
 *
 * @author marco
 */
@Stateless
public class DroneOwnerFacade extends AbstractFacade<DroneOwner> implements DroneOwnerFacadeLocal {

    @PersistenceContext(unitName = "pickitup-PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public DroneOwnerFacade() {
        super(DroneOwner.class);
    }
    
    @Override
    public List<DroneOwner> getSurroundingDroneOwners(LatLng positionClient) {
        DroneOwner do1 = new DroneOwner();
        DroneOwner do2 = new DroneOwner();
        DroneOwner do3 = new DroneOwner();
        LatLng l1 = new LatLng();
        LatLng l2 = new LatLng();
        LatLng l3 = new LatLng();
        l1.setLatitude(45.087547);
        l1.setLongitude(7.656822);
        l2.setLatitude(45.093304);
        l2.setLongitude(7.656993);
        l3.setLatitude(45.096811);
        l3.setLongitude(7.671413);
        do1.setPosition(l1);
        do2.setPosition(l2);
        do3.setPosition(l3);
        List<DroneOwner> l = new ArrayList();
        l.add(do1);
        l.add(do2);
        l.add(do3);    
        return l;
    }
    
}
