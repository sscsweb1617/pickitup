/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.facade;

import model.entity.Parcel;
import java.util.*;
import javax.ejb.*;
import model.*;
import model.entity.Client;

/**
 *
 * @author marco
 */
@Local
public interface ParcelFacadeLocal {

    void create(Parcel parcel);

    void edit(Parcel parcel);

    void remove(Parcel parcel);

    Parcel find(Object id);

    List<Parcel> findAll();

    List<Parcel> findRange(int[] range);

    int count();

    List<Parcel> getUserParcelList(Client client);
    
}
