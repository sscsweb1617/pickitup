/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.facade;

import java.util.List;
import model.entity.AppUser;
import javax.ejb.*;
import javax.persistence.*;

/**
 *
 * @author marco
 */
@Stateless
public class AppUserFacade extends AbstractFacade<AppUser> implements AppUserFacadeLocal {

    @PersistenceContext(unitName = "pickitup-PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AppUserFacade() {
        super(AppUser.class);
    }

    /**
     *
     * @param email
     * @return
     */
    @Override
    public AppUser findUserByEmail(String email) {
        List<AppUser> results = em.createQuery(
                "SELECT c FROM AppUser c WHERE c.email LIKE :email")
                .setParameter("email", email)
                .setMaxResults(10)
                .getResultList();
        if (results.size() > 1) {
            throw new IllegalStateException("The email address '" + email + "' is used by more than one user!");
        }
        return results.isEmpty() ? null : results.get(0);
    }

}
