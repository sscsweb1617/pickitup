/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.facade;

import model.entity.Delivery;
import java.util.*;
import javax.ejb.*;
import model.entity.AppUser;
import model.entity.Drone;
import model.entity.Parcel;

/**
 *
 * @author marco
 */
@Local
public interface DeliveryFacadeLocal {

    void create(Delivery delivery);

    void edit(Delivery delivery);

    void remove(Delivery delivery);

    Delivery find(Object id);

    List<Delivery> findAll();

    List<Delivery> findRange(int[] range);

    int count();

    List<Delivery> findDroneOwnerDeliveries(AppUser user);

    void cleanProposals();

    List<Delivery> findClientDeliveries(AppUser user);
    
    List<Delivery> deliveryWithThisParcel(Parcel parcel); 
    
    List<Delivery> deliveryWithThisDrone(Drone drone);
}
