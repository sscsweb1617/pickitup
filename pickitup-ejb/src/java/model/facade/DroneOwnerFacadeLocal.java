/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.facade;

import model.entity.DroneOwner;
import java.util.*;
import javax.ejb.*;
import model.*;
import model.entity.LatLng;

/**
 *
 * @author marco
 */
@Local
public interface DroneOwnerFacadeLocal {

    void create(DroneOwner droneOwner);

    void edit(DroneOwner droneOwner);

    void remove(DroneOwner droneOwner);

    DroneOwner find(Object id);

    List<DroneOwner> findAll();

    List<DroneOwner> findRange(int[] range);

    int count();
    
    List<DroneOwner> getSurroundingDroneOwners(LatLng positionClient);
    
}
