/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.facade;

import model.entity.LatLng;
import javax.ejb.*;
import javax.persistence.*;
import model.*;

/**
 *
 * @author marco
 */
@Stateless
public class LatLngFacade extends AbstractFacade<LatLng> implements LatLngFacadeLocal {

    @PersistenceContext(unitName = "pickitup-PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public LatLngFacade() {
        super(LatLng.class);
    }
    
}
