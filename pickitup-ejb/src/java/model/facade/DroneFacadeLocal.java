/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.facade;

import model.entity.Drone;
import java.util.*;
import javax.ejb.*;
import model.entity.*;

/**
 *
 * @author marco
 */
@Local
public interface DroneFacadeLocal {

    void create(Drone drone);

    void edit(Drone drone);

    void remove(Drone drone);

    Drone find(Object id);

    List<Drone> findAll();

    List<Drone> findRange(int[] range);

    int count();

    List<Drone> getDronesOwnedBy(DroneOwner owner);

    public List<Drone> findDronesWithMax(double volume, double weight);
    
}
