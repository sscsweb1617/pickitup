package ctrl;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.IOException;
import java.security.GeneralSecurityException;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.entity.AppUser;

/**
 * Servlet that manages login and signup procedures via HTTP.
 */
@WebServlet(urlPatterns = {"/SignupServlet"})
public class SignupServlet extends GenericServlet {

    @EJB
    private SignupBeanLocal signupBean;

    private void googleLogin(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        if (getSessionUser(request) != null) {
            response.sendRedirect("/pickitup-war/frontend/index.html");
        }
        try {
            String idToken = request.getParameter("idtoken");
            AppUser user = signupBean.validateGoogleUser(idToken, true);
            if (user == null) {
                throw new ServletException("Cant validate user");
            }

            String typeLogin = "google";
            setupSession(user, typeLogin, request, response);
        } catch (ServletException | IOException | GeneralSecurityException e) {
            //msg inviato nella richiesta http
            request.setAttribute("msg", e.getMessage());
            request.getRequestDispatcher("/admin/500.jsp").forward(request, response);
        }
    }

    private void facebookLogin(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        if (getSessionUser(request) != null) {
            response.sendRedirect("/pickitup-war/frontend/index.html");
        }
        String idToken = request.getParameter("idtoken");

        AppUser user = signupBean.validateFacebookUser(idToken, true);

        String typeLogin = "facebook";
        setupSession(user, typeLogin, request, response);
    }

    private void setupSession(AppUser user, String typeLogin, HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String address;
        request.getSession().setAttribute("user", user);
        if (user.getClass().equals(AppUser.class)) {
            address = "/admin/setAppUser.jsp";
        } else {
            address = "/admin/index.jsp";
        }
        request.getSession().setAttribute("typeLogin", typeLogin);
        request.getRequestDispatcher(address).forward(request, response);
    }

    private void setAppUser(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        String chosenRole = request.getParameter("optradio");
        AppUser newUser = signupBean.changeUserPrivilege(getSessionUser(request).getId(), chosenRole);
        request.getSession().setAttribute("user", newUser);
        request.getRequestDispatcher("/admin/index.jsp").forward(request, response);
    }

    private void logout(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (getSessionUser(request) != null) {
            //if(request.getSession()!=null)
            try {
                request.getSession().invalidate();
            } catch (NullPointerException ne) {

            }
        }
        response.sendRedirect("/pickitup-war/frontend/index.html");
    }

    private void login(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        if (getSessionUser(request) == null) {
            response.sendRedirect("/pickitup-war/admin/signin.html");
        } else {
            request.getRequestDispatcher("/admin/index.jsp").forward(request, response);
        }
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            String action = request.getParameter("action");
            switch (action == null ? "" : action) {
                case "login":
                    login(request, response);
                    break;
                case "login-g":
                    googleLogin(request, response);
                    break;
                case "login-f":
                    facebookLogin(request, response);
                    break;
                case "setAppUser":
                    setAppUser(request, response);
                    break;
                case "logout":
                    logout(request, response);
                    break;
            }
        } catch (IOException | ServletException e) {
            //msg inviato nella richiesta http
            request.setAttribute("msg", e.getMessage());
            request.getRequestDispatcher("/admin/500.jsp").forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
