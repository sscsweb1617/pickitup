/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ctrl;

import java.io.*;
import java.util.*;
import javax.ejb.*;
import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import model.entity.*;
import model.facade.CoverageFacadeLocal;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 * Servlet that manages drones via HTTP.
 */
@WebServlet(name = "DroneServlet", urlPatterns = {"/DroneServlet"})
public class DroneServlet extends GenericServlet {

    //File tresholds and parameters
    private static final String DATA_DIRECTORY = "data";
    private static final int MAX_MEMORY_SIZE = 1024 * 1024 * 2;
    private static final int MAX_REQUEST_SIZE = 1024 * 1024;

    @EJB
    private DroneBeanLocal droneManager;
    
    @EJB
    private CoverageFacadeLocal coverageManager;
    
    
    private void add(HttpServletRequest request, HttpServletResponse response, HashMap<String,String> params) throws ServletException, IOException {
        if(getSessionUser(request) == null || !(getSessionUser(request) instanceof DroneOwner)) {
            throw new ServletException("Session expired");
        }

        //params.forEach((k,v)->System.out.println(k+", "+v));
        
        DroneOwner user = (DroneOwner)getSessionUser(request);
        
        String name = params.get("name");
        Double weight = Double.parseDouble(params.get("weight"));
        Double speed = Double.parseDouble(params.get("speed"));
        Double flatFee = Double.parseDouble(params.get("flatFee"));
        Double perKmFee = Double.parseDouble(params.get("perKmFee"));
        int autonomy = Integer.parseInt(params.get("autonomy"));
        
        Coverage coverage = new Coverage();
        coverage.setCenter(user.getPosition());
        Double radius = Double.parseDouble(params.get("coverage"));
        
        coverage.setRadius(radius);
        
        coverageManager.create(coverage);
        
        Double maxPayload = Double.parseDouble(params.get("maxPayload"));
        Double maxVolume = Double.parseDouble(params.get("maxVolume"));
        
        
        Drone drone = droneManager.addDrone(params.get("imgURL"), name, weight, speed, autonomy, coverage, maxPayload, maxVolume, flatFee, perKmFee, user);
        user.getDroneList().add(drone);
        request.getSession().setAttribute("user", user);
        request.getSession().setAttribute("action", "");
        request.getRequestDispatcher("/DroneServlet").forward(request, response);
    }

    private void changeImage(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        throw new ServletException("Not yet implemented!!");/*
        if(getSessionUser(request) == null || !(getSessionUser(request) instanceof DroneOwner)) {
            throw new ServletException("Non puoi");
        }
        // TODO gestire upload!!(?)
        long id = Long.parseLong(request.getParameter("id"));
        String img = request.getParameter("image");
        droneManager.changeDroneImage(id, img);*/
    }

    private void delete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if(getSessionUser(request) == null || !(getSessionUser(request) instanceof DroneOwner)) {
            throw new ServletException("Session expired");
        }
        
        DroneOwner user = (DroneOwner)getSessionUser(request);
        
        Long id = Long.parseLong(request.getParameter("droneId"));
        
        Drone drone = droneManager.getDrone(id);
        
        if(drone == null)
            throw new ServletException("Cannot find this drone");
        
        
        if(!drone.getOwner().equals(user))
            throw new ServletException("You are trying to delete someone else's drone...");
        
        boolean success = droneManager.deleteDrone(id);
        
        if(success) {
            user.getDroneList().remove(drone);
            request.setAttribute("drones", user.getDroneList());
            request.getRequestDispatcher("/admin/my-drones.jsp").forward(request, response);
        } else {
            request.setAttribute("message", "error");
            request.setAttribute("drones", user.getDroneList());
            request.getRequestDispatcher("/admin/my-drones.jsp").forward(request, response);
        }
    }

    private void list(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if(getSessionUser(request) == null || !(getSessionUser(request) instanceof DroneOwner)) {
            throw new ServletException("Session expired");
        }
        DroneOwner owner = (DroneOwner)getSessionUser(request);
        
        List<Drone> drones = droneManager.getDroneList(owner);
        
        request.setAttribute("drones", drones);
        request.getRequestDispatcher("/admin/my-drones.jsp").forward(request, response);
    }

    private void rename(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        if(getSessionUser(request) == null || !(getSessionUser(request) instanceof DroneOwner)) {
            throw new ServletException("Session expired");
        }
        long id = Long.parseLong(request.getParameter("droneId"));
        String name = request.getParameter("name");
        droneManager.renameDrone(id, name);
    }
    
    private void edit(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if(getSessionUser(request) == null || !(getSessionUser(request) instanceof DroneOwner)) {
            throw new ServletException("Session expired");
        }
        DroneOwner user = (DroneOwner)getSessionUser(request);
        
        Long id = Long.parseLong(request.getParameter("droneId"));
        
        Drone drone = droneManager.getDrone(id);
        
        if(drone == null)
            throw new ServletException("Cannot find this drone");
        
        
        if(!drone.getOwner().equals(user))
            throw new ServletException("You are trying to modify someone else's drone...");
        
        rename(request, response);
        setFees(request, response);
        list(request,response);
    }

    private void setFees(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        if(getSessionUser(request) == null || !(getSessionUser(request) instanceof DroneOwner)) {
            throw new ServletException("Session expired");
        }
        Long id = Long.parseLong(request.getParameter("droneId"));
        
        Double flatFee = Double.parseDouble(request.getParameter("flatFee"));
        if(flatFee.compareTo(0.00) <= 0)
            throw new ServletException("Flat fee can't be empty neighter negative");
        
        Double perKmFee = Double.parseDouble(request.getParameter("perKmFee"));
        if(perKmFee.compareTo(0.00) <= 0)
            throw new ServletException("Per km fee can't be empty neighter negative");
        
        droneManager.setDroneFees(id, flatFee, perKmFee);
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request  servlet request
     * @param response servlet response
     *
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            HashMap<String,String> params = null;
            String action;

            if(ServletFileUpload.isMultipartContent(request)) {
                params = parseRequestData(request,response);
                action = params.get("action");
            } else {
                action = request.getParameter("action");
            }
            switch(action == null ? "" : action) {
                case "add":
                    if(params==null)
                        throw new ServletException("Something went wrong with data sending/recieving");
                    add(request, response, params);
                    break;
                case "delete":
                    delete(request, response);
                    break;
                case "edit":
                    edit(request, response);
                    break;
                case "img":
                    changeImage(request, response);
                    break;
                default:
                    list(request, response);
                    break;
            }
        }catch(ServletException e) {
            request.setAttribute("message", e.getMessage());
            request.getRequestDispatcher("/admin/500.jsp").forward(request,response);
        }catch(IOException e) {
            request.setAttribute("message", e.getMessage());
            request.getRequestDispatcher("/admin/500.jsp").forward(request,response);
        } finally {
            return;
        }
    }
    
    private HashMap<String,String> parseRequestData(HttpServletRequest request, HttpServletResponse response)
            throws ServletException {
   
        HashMap<String,String> params = new HashMap<>();
        
        // Create a factory for disk-based file items
        DiskFileItemFactory factory = new DiskFileItemFactory();

        // Sets the size threshold beyond which files are written directly to
        // disk.
        factory.setSizeThreshold(MAX_MEMORY_SIZE);

        // Sets the directory used to temporarily store files that are larger
        // than the configured size threshold. We use temporary directory for
        // java
        factory.setRepository(new File(System.getProperty("java.io.tmpdir")));

        // constructs the folder where uploaded file will be stored
        String uploadFolder = getServletContext().getRealPath("")
                + File.separator + DATA_DIRECTORY;

        // Create a new file upload handler
        ServletFileUpload upload = new ServletFileUpload(factory);

        // Set overall request size constraint
        upload.setSizeMax(MAX_REQUEST_SIZE);
        
        try {
            // Parse the request
            List items = upload.parseRequest(request);
            Iterator iter = items.iterator();
            while (iter.hasNext()) {
                FileItem item = (FileItem) iter.next();

                if (!item.isFormField()) {
                    String filePath;
                    String fileName;
                    do {
                        fileName = + getSessionUser(request).getId() + new File(item.getName()).getName() + new Random().nextInt(1024);
                        filePath = uploadFolder + File.separator + fileName;
                    }while(new File(filePath).exists());
                    File uploadedFile = new File(filePath);
                    params.put("imgURL", DATA_DIRECTORY + "/" + fileName);
                    // saves the file to upload directory
                    item.write(uploadedFile);
                } else {
                    params.put(item.getFieldName(), item.getString());
                }
            }
        } catch (Exception e) {
            throw new ServletException(e.getMessage());
        }
        return params;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     *
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     *
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
