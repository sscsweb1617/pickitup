/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ctrl.webservice;

import ctrl.DeliveryBeanLocal;
import ctrl.SignupBeanLocal;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJBException;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.core.Context;
import javax.ws.rs.Produces;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import model.entity.AppUser;
import model.entity.Delivery;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * REST Web Service
 *
 * @author marco
 */
@Path("/")
public class MobileWebService {

    DeliveryBeanLocal deliveryBean = lookupDeliveryBeanLocal();

    SignupBeanLocal signupBean = lookupSignupBeanLocal();

    @Context
    private HttpServletRequest req;

    /**
     * Verifies a login token and eventually logs into te system.
     *
     * @param type The login type (Facebook or Google)
     * @param token The login token to verify
     * @return the logged user details, in JSON format
     */
    @GET
    @Path("login/{type}/{token}")
    @Produces(MediaType.APPLICATION_JSON)
    public String login(
            @PathParam("type") String type,
            @PathParam("token") String token) {
        AppUser user = null;
        String message = null;
        try {
            switch (type) {
                case "google":
                    user = signupBean.validateGoogleUser(token, false);
                    break;
                case "facebook":
                    user = signupBean.validateFacebookUser(token, false);
                    break;
                default:
                    throw new IllegalArgumentException();
            }
        } catch (EJBException | IllegalArgumentException | IOException | GeneralSecurityException e) {
            message = e.getMessage();
        }

        JSONObject json = new JSONObject();
        if (user == null) {
            json.put("error", message == null ? "Invalid call, token or user!" : message);
        } else {
            req.getSession().setAttribute("user", user);
            json.put("JSESSIONID", req.getSession().getId());
            json.put("email", user.getEmail());
            json.put("name", user.getName() + " " + user.getSurname());
        }
        return json.toString();
        /*
        JSONObject json = new JSONObject();
        json.put("JSESSIONID", req.getSession().getId());
        json.put("email", "utente@fake.com");
        json.put("name", "Fabio Falso");
        return json.toString();
         */
    }

    /**
     * @return a list of all the deliveries in which the actual session's user
     * is concerned.
     */
    @GET
    @Path("deliveries")
    @Produces(MediaType.APPLICATION_JSON)
    public String deliveryList() {
        AppUser user = (AppUser) req.getSession().getAttribute("user");
        List<Delivery> deliveries = deliveryBean.getUserDeliveryList(user);

        JSONArray jsonList = new JSONArray();
        deliveries.stream()
                .map(d -> {
                    JSONObject jsonDelivery = new JSONObject();
                    jsonDelivery.put("id", d.getId());
                    jsonDelivery.put("name", d.getParcel().getName());
                    jsonDelivery.put("drone", d.getDrone().getName());
                    jsonDelivery.put("destination", d.getDestination().toJson());
                    return jsonDelivery;
                })
                .forEach(delivery -> jsonList.put(delivery));

        return jsonList.toString();
        /*
        JSONArray jsonList = new JSONArray();
        for (int i = 0; i < 3; i++) {
            JSONObject jsonDelivery = new JSONObject();
            jsonDelivery.put("name", "Parcel " + i);
            jsonDelivery.put("drone", "Drone " + i);
            jsonDelivery.put("destination", LatLng.random().toJson());
            jsonList.put(jsonDelivery);
        }
        return jsonList.toString();
         */
    }

    /**
     * @param id the id of a delivery
     * @return the details of the delivery in JSON format
     */
    @GET
    @Path("delivery/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getDelivery(
            @PathParam("id") long id) {
        AppUser user = (AppUser) req.getSession().getAttribute("user");
        Delivery delivery = deliveryBean.getDelivery(id);
        JSONObject jsonDelivery = new JSONObject();

        if (delivery.getClient().equals(user) || delivery.getDrone().getOwner().equals(user)) {
            jsonDelivery.put("name", "Parcel " + delivery.getParcel().getName());
            jsonDelivery.put("status", delivery.getStatus());
            jsonDelivery.put("status_desc", delivery.getStatus().getDescription());

            jsonDelivery.put("drone", delivery.getDrone().getName());
            jsonDelivery.put("client", delivery.getClient().toString());
            jsonDelivery.put("cost", delivery.getCost());
            jsonDelivery.put("estimated_time", delivery.getEstimatedTime());

            jsonDelivery.put("initial_position", delivery.getInitialPosition().toJson());
            jsonDelivery.put("acceptance_position", delivery.getDronePosition().toJson());
            jsonDelivery.put("destination", delivery.getDestination().toJson());
        } else {
            jsonDelivery.put("error", "This delivery is not yours!");
        }
        return jsonDelivery.toString();
        /*
        JSONObject jsonDelivery = new JSONObject();
        
        jsonDelivery.put("name", "Parcel p1");
        jsonDelivery.put("status", Delivery.Status.SENT);
        jsonDelivery.put("status_desc", "Your parcel is flying now! :D");
        
        jsonDelivery.put("drone", "elicottero");
        jsonDelivery.put("client", "Antonio");
        jsonDelivery.put("cost", 3.55);
        jsonDelivery.put("estimated_time", 68);
        
        jsonDelivery.put("initial_position", LatLng.random().toJson());
        jsonDelivery.put("acceptance_position", LatLng.random().toJson());
        jsonDelivery.put("destination", LatLng.random().toJson());
        
        return jsonDelivery.toString();
         */
    }

    /**
     * Debug method.
     * @return the current sessionid
     */
    @GET
    @Path("sessionid")
    @Produces(MediaType.APPLICATION_JSON)
    public String sessionId() {
        JSONObject json = new JSONObject();
        json.put("actual id", req.getSession().getId());
        return json.toString();
    }

    private SignupBeanLocal lookupSignupBeanLocal() {
        try {
            javax.naming.Context c = new InitialContext();
            return (SignupBeanLocal) c.lookup("java:global/pickitup/pickitup-ejb/SignupBean!ctrl.SignupBeanLocal");
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }

    private DeliveryBeanLocal lookupDeliveryBeanLocal() {
        try {
            javax.naming.Context c = new InitialContext();
            return (DeliveryBeanLocal) c.lookup("java:global/pickitup/pickitup-ejb/DeliveryBean!ctrl.DeliveryBeanLocal");
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }
}
