/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ctrl;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * The servlet that handles the left-side menu.
 */
@WebServlet(name = "UserMenu", urlPatterns = {"/UserMenu"})
public class UserMenu extends GenericServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if (getSessionUser(request) == null) {
            throw new ServletException("Non sei loggato!");
        }
        try {
            String action = request.getParameter("action");
            switch (action == null ? "" : action) {
                case "profile":
                    request.getRequestDispatcher("/ProfileServlet").forward(request, response);
                    break;
                case "deliveries":
                    request.getRequestDispatcher("/DeliveryServlet").forward(request, response);
                    break;
                case "drones":
                    request.getRequestDispatcher("/DroneServlet").forward(request, response);
                    break;
                case "in-zone":
                    request.getRequestDispatcher("/DroneServlet?action=in-zone").forward(request, response);
                    break;
                case "parcels":
                    request.getRequestDispatcher("/ParcelServlet").forward(request, response);
                    break;
                default:
                    request.getRequestDispatcher("/admin/index.jsp").forward(request, response);

            }
        } catch (IOException | ServletException e) {
            //msg inviato nella richiesta http
            request.setAttribute("msg", e.getMessage());
            request.getRequestDispatcher("/admin/500.jsp").forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
