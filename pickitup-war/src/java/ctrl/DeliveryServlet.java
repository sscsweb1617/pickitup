/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ctrl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.entity.AppUser;
import model.entity.Client;
import model.entity.Delivery;
import model.entity.DroneOwner;
import model.entity.LatLng;

/**
 * Servlet that manages deliveries via HTTP.
 */
@WebServlet(name = "DeliveryServlet", urlPatterns = {"/DeliveryServlet"})
public class DeliveryServlet extends GenericServlet {

    @EJB
    private DeliveryBeanLocal deliveryBean;

    private void getAllDeliveries(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        AppUser user = getSessionUser(request);
        if (user == null) {
            throw new ServletException("Non sei loggato!");
        }

        List<Delivery> deliveries = deliveryBean.getUserDeliveryList(user);
        request.setAttribute("deliveries", deliveries);
        request.getRequestDispatcher("/admin/my-deliveries.jsp").forward(request, response);
    }

    private void getSurroundingDroneOwners(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        Client client = (Client) getSessionUser(request);
        if (client == null) {
            throw new ServletException("Non sei loggato!");
        }
        long id = client.getId();
        LatLng positionClient = client.getPosition();
        List<DroneOwner> listDroneOwners = deliveryBean.getSurroundingDroneOwners(positionClient);
        request.setAttribute("drone-owners", listDroneOwners);
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            String action = request.getParameter("action");
            switch (action == null ? "" : action) {
                case "search":
                    searchDeliveries(request, response);
                    break;
                case "book":
                    bookDelivery(request, response);
                    break;
                case "deliveryAccepted":
                    acceptDelivery(request, response);
                    break;
                case "deliverySent":
                    sendDelivery(request, response);
                    break;
                case "deliveryDone":
                    doneDelivery(request, response);
                    break;
                default:
                    getAllDeliveries(request, response);
                    break;
            }
        } catch (IOException | ServletException e) {
            //msg inviato nella richiesta http
            request.setAttribute("msg", e.getMessage());
            request.getRequestDispatcher("/admin/500.jsp").forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void searchDeliveries(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (getSessionUser(request) == null || getSessionUser(request) instanceof DroneOwner) {
            throw new ServletException("Solo un Client può cercare nuove consegne!");
        }
        Client user = (Client) getSessionUser(request);
        request.setAttribute("parcels", user.getParcelList());

        List<Delivery> deliveries;
        boolean searchResult;
        if (request.getParameter("parcel") != null) {
            long parcelId = Long.parseLong(request.getParameter("parcel"));
            double destLat = Double.parseDouble(request.getParameter("destLat"));
            double destLng = Double.parseDouble(request.getParameter("destLng"));
            deliveries = deliveryBean.searchDeliveries(user,
                    parcelId, destLat, destLng);
            searchResult = true;
        } else {
            deliveries = new ArrayList<>();
            searchResult = false;
        }
        request.setAttribute("deliveries", deliveries);
        request.setAttribute("searchResult", searchResult);

        request.getRequestDispatcher("/admin/search-deliveries.jsp").forward(request, response);
    }

    private void bookDelivery(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if (getSessionUser(request) == null || getSessionUser(request) instanceof DroneOwner) {
            throw new ServletException("You are not a Client!");
        }
        long id = Long.parseLong(request.getParameter("id"));

        Delivery delivery = deliveryBean.getDelivery(id);
        if (!delivery.getClient().equals(getSessionUser(request))) {
            throw new IllegalArgumentException("This delivery is not yours!");
        }
        deliveryBean.updateStatus(delivery, Delivery.Status.BOOKED);
        deliveryBean.cleanProposals();

        getAllDeliveries(request, response);
    }

    private void acceptDelivery(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        AppUser user = getSessionUser(request);
        if (user == null) {
            throw new ServletException("You are not logged in!");
        }
        long id = Long.parseLong(request.getParameter("id"));

        Delivery delivery = deliveryBean.getDelivery(id);
        if (!(delivery.getClient().equals(user) || delivery.getDrone().getOwner().equals(user))) {
            throw new IllegalArgumentException("You can't update this delivery!");
        }
        deliveryBean.updateStatus(delivery, Delivery.Status.ACCEPTED);
        deliveryBean.cleanProposals();

        getAllDeliveries(request, response);
    }

    private void sendDelivery(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        AppUser user = getSessionUser(request);
        if (user == null) {
            throw new ServletException("You are not logged in!");
        }
        long id = Long.parseLong(request.getParameter("id"));

        Delivery delivery = deliveryBean.getDelivery(id);
        if (!delivery.getDrone().getOwner().equals(user)) {
            throw new IllegalArgumentException("You can't update this delivery!");
        }
        deliveryBean.updateStatus(delivery, Delivery.Status.SENT);
        deliveryBean.cleanProposals();

        getAllDeliveries(request, response);
    }

    private void doneDelivery(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        AppUser user = getSessionUser(request);
        if (user == null) {
            throw new ServletException("You are not logged in!");
        }
        long id = Long.parseLong(request.getParameter("id"));

        Delivery delivery = deliveryBean.getDelivery(id);
        if (!delivery.getDrone().getOwner().equals(user)) {
            throw new IllegalArgumentException("You can't update this delivery!");
        }
        deliveryBean.updateStatus(delivery, Delivery.Status.DONE);
        deliveryBean.cleanProposals();

        getAllDeliveries(request, response);
    }

}
