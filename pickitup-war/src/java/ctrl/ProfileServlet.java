/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ctrl;

import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.entity.AppUser;

/**
 * Servlet that manages profile modification procedures via HTTP.
 */
@WebServlet(name = "ProfileServlet", urlPatterns = {"/ProfileServlet"})
public class ProfileServlet extends GenericServlet {

    @EJB
    private ProfileBeanLocal profileBean;

    private void editProfile(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        AppUser user = getSessionUser(request);
        if (user == null) {
            throw new ServletException("Non sei loggato!");
        }
        String what = request.getParameter("what");
        String value = request.getParameter("value");
        boolean success = false;
        switch (what) {
            case "name":
                success = profileBean.setName(user.getId(), value);
                if (success) {
                    user.setName(value);
                }
                break;
            case "surname":
                success = profileBean.setSurname(user.getId(), value);
                if (success) {
                    user.setSurname(value);
                }
                break;
            case "paypal":
                success = profileBean.setPaypal(user.getId(), value);
                if (success) {
                    user.setPaypal(value);
                }
                break;
            case "phone":
                success = profileBean.setPhoneNumber(user.getId(), value);
                if (success) {
                    user.setPhoneNumber(value);
                }
                break;
        }
        if (success) {
            request.getSession().setAttribute("user", user);
        }
        response.setContentType("text/plain");
        response.setCharacterEncoding("UTF-8"); // You want world domination, huh?
        response.getWriter().write(success ? value : "failure");

    }

    private void viewProfile(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        AppUser user = getSessionUser(request);
        if (user == null) {
            throw new ServletException("Non sei loggato!");
        }
        request.getRequestDispatcher("/admin/profile.jsp").forward(request, response);

    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            String action = request.getParameter("action");
            switch(action == null ? "" : action) {
                case "editprofile":
                    editProfile(request, response);
                    break;
                case "setPosition":
                    setPosition(request, response);
                    break;
                default:
                    viewProfile(request, response);
            }
        } catch (IOException | ServletException e) {
            //msg inviato nella richiesta http
            request.setAttribute("msg", e.getMessage());
            request.getRequestDispatcher("/admin/500.jsp").forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void setPosition(HttpServletRequest request, HttpServletResponse response) {
        if(getSessionUser(request) == null) {
            throw new IllegalStateException("You are not logged in");
        }
        double latitude = Double.parseDouble(request.getParameter("latitude"));
        double longitude = Double.parseDouble(request.getParameter("longitude"));
        profileBean.setPosition(getSessionUser(request), latitude, longitude);
    }

}
