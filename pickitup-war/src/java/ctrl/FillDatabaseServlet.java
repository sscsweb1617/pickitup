/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ctrl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.entity.Coverage;
import model.entity.Drone;
import model.entity.DroneOwner;
import model.entity.LatLng;
import model.facade.CoverageFacadeLocal;
import model.facade.DroneFacadeLocal;
import model.facade.DroneOwnerFacadeLocal;
import model.facade.LatLngFacadeLocal;

/**
 * Just a database filler servlet, used to fastly create fake data.
 * @author gianlucaturin
 */
@WebServlet(name = "FillDatabaseServlet", urlPatterns = {"/FillDatabaseServlet"})
public class FillDatabaseServlet extends HttpServlet {

    @EJB
    private LatLngFacadeLocal latLngFacade;

    @EJB
    private DroneFacadeLocal droneFacade;

    @EJB
    private CoverageFacadeLocal coverageFacade;

    @EJB
    private DroneOwnerFacadeLocal droneOwnerFacade;

    private final String[] names = {"Pietro", "Giovanni", "Ida", "Ugo", "Michele",
        "Luca", "Marco", "Anna", "Lucia", "Francesca",
        "Valentina", "Sara", "Guido", "Paola", "Paolo"};
    private final String[] surnames = {"Volo", "Rossi", "Mieli", "Flotta", "Elica", "Mosca", "Zocca", "Martini", "Lesti", "Mora", "Costanzo", "Rapidi", "Vespa", "Pilota", "Cielo"};

    void generateTwentyFiveDroneOwners(int start, int end) {
        DroneOwner d;
        Random r = new Random();
        for (int i = start; i < end; i++) {
            d = new DroneOwner();
            d.setName(i < 15 ? names[i] : names[r.nextInt(15)] + " " + names[r.nextInt(15)]);
            d.setSurname(i < 15 ? surnames[i] : "Da" + surnames[r.nextInt(15)]);
            d.setEmail((d.getName().replace(" ", ".") + "." + d.getSurname() + i + "@gmail.com").toLowerCase());
            d.setPaypal(d.getEmail());

            String num = "3";
            for (int j = 0; j < 9; j++) {
                num += r.nextInt(10);
            }
            d.setPhoneNumber(num);

            LatLng ll = LatLng.random();
            latLngFacade.create(ll);
            d.setPosition(ll);
            d.setIMG("http://lorempixel.com/300/300/people");

            ArrayList<Drone> drones = new ArrayList<>();
            Drone drone = new Drone();
            drone.setImage("data/" + i + ".jpg");
            String[] prefix = {"Super", "Mega", "Iper", "Special"};
            drone.setName(prefix[r.nextInt(4)] + " " + r.nextInt(9999));
            drone.setAutonomy(r.nextInt(15) + 15);
            drone.setMaxPayload((r.nextInt(15) + 20) / 10.0);
            drone.setSpeed(r.nextInt(45) + 35.0);
            drone.setFlatFee((r.nextInt(10) + 5) / 10.0);
            drone.setPerKmFee((r.nextInt(20) + 5) / 100.0);
            drone.setWeight((r.nextInt(100) + 50) / 100.0);
            drone.setOwner(d);
            Coverage cov = new Coverage();
            cov.setCenter(d.getPosition());
            cov.setRadius((drone.getSpeed() / 3.6) * drone.getAutonomy() * 60);
            coverageFacade.create(cov);
            drone.setCoverage(cov);
            drone.setMaxVolume((r.nextInt(250) + 250) * 1000);
            drones.add(drone);
            droneOwnerFacade.create(d);
            droneFacade.create(drone);
            d.setDroneList(drones);
            droneOwnerFacade.edit(d);
        }
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ExecutorService executor = Executors.newFixedThreadPool(5);
        for (int s = 0, e = 5; s < 25; s += e) {
            Runnable worker = new WorkerThread(s, s + e);
            executor.execute(worker);
        }
        executor.shutdown();
        while (!executor.isTerminated()) {
        }

        request.getRequestDispatcher("/UserMenu").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public class WorkerThread implements Runnable {

        private final int start, end;

        public WorkerThread(int s, int e) {
            start = s;
            end = e;
        }

        @Override
        public void run() {
            generateTwentyFiveDroneOwners(start, end);
        }

        @Override
        public String toString() {
            return "Da " + start + " a " + end;
        }
    }

}
