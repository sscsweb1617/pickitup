/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ctrl;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.entity.Client;
import model.entity.Parcel;

/**
 * Servlet that manages parcels via HTTP.
 */
@WebServlet(name = "ParcelServlet", urlPatterns = {"/ParcelServlet"})
public class ParcelServlet extends GenericServlet {

    @EJB
    private ParcelBeanLocal parcelBean;

    private void createNewParcel(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        String name = request.getParameter("name");
        double volume = Double.parseDouble(request.getParameter("volume"));
        double estimatedValue = Double.parseDouble(request.getParameter("estimatedValue"));
        double weight = Double.parseDouble(request.getParameter("weight"));
        parcelBean.create((Client) getSessionUser(request), name, estimatedValue, volume, weight);
    }

    private void getParcels(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        Client client = (Client) getSessionUser(request);
        if (client == null) {
            throw new ServletException("Non sei loggato!");
        }

        List<Parcel> listParcels = parcelBean.getUserParcelList(client);
        client.setParcelList(listParcels);

        String action = request.getParameter("action");
        if (action != null && action.equals("parcelFromHome")) {
            request.setAttribute("modalOpened", "modal-open");
            String nameParcel = request.getParameter("nameParcel");
            request.setAttribute("nameParcelFromHome", nameParcel);
        }

        request.getRequestDispatcher("/admin/my-parcels.jsp").forward(request, response);
    }

    private void deleteParcel(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        Client client = (Client) getSessionUser(request);
        if (client == null) {
            throw new ServletException("Non sei loggato!");
        }

        Long id = Long.parseLong(request.getParameter("idParcel"));

        Parcel parcel = parcelBean.getParcel(id);
        if (!(parcel.getOwner().equals(client))) {
            throw new IllegalArgumentException("You can't remove this parcel!");
        }

        boolean statusParcel = parcelBean.controlStatusParcel(id);
        if (!statusParcel) {
            throw new IllegalArgumentException("You can't remove this parcel!");
        }

        parcelBean.deleteParcel(id);
    }

    private void controlStatusParcel(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        Client client = (Client) getSessionUser(request);
        if (client == null) {
            throw new ServletException("Non sei loggato!");
        }

        Long id = Long.parseLong(request.getParameter("idParcel"));

        boolean deleteButton = parcelBean.controlStatusParcel(id);

        try (PrintWriter out = response.getWriter()) {
            out.print(deleteButton);
        }
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            String action = request.getParameter("action");
            switch (action == null ? "" : action) {
                case "new":
                    createNewParcel(request, response);
                    getParcels(request, response);
                    return;
                case "parcelFromHome":
                    getParcels(request, response);
                    return;
                case "deleteParcel":
                    deleteParcel(request, response);
                    getParcels(request, response);
                    return;
                case "controlStatusParcel":
                    controlStatusParcel(request, response);
                    return;
                default:
                    getParcels(request, response);
            }
        } catch (IOException | ServletException e) {
            //msg inviato nella richiesta http
            request.setAttribute("msg", e.getMessage());
            request.getRequestDispatcher("/admin/500.jsp").forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
