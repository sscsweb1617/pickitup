/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ctrl;

import javax.servlet.http.*;
import model.entity.*;

/**
 *
 * @author marco
 */
public abstract class GenericServlet extends HttpServlet {

    AppUser getSessionUser(HttpServletRequest request) {
        HttpSession session = request.getSession();
        return (AppUser) session.getAttribute("user");
    }
}
