<%@page import="model.entity.AppUser"%>
<jsp:include page="../WEB-INF/index-structure-pre.jsp"/>
<jsp:useBean id="user" class="model.entity.AppUser" scope="session" />
<script src="${pageContext.request.contextPath}/admin/scripts/profile.js"></script>

<script>
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>

<!-- content wrapper -->
<div class="content-wrap">

    <!-- inner content wrapper -->
    <div class="wrapper">
        <div class="row">
            <div class="col-md-12">
                <!-- profile information sidebar -->

                <div class="panel overflow-hidden no-b profile p15">

                    <div class="row mb25">
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-8">
                                    <h4 class="mb0"> <jsp:getProperty name="user" property="name"/> <b><jsp:getProperty name="user" property="surname"/></b></h4>
                                    <small><jsp:getProperty name="user" property="class"/></small>

                                    <h6 class="mt15 mb15">Human Resources Manager</h6>
                                    <ul class="user-meta">
                                        <li>
                                            <i class="ti-email mr5"></i>
                                            <span><jsp:getProperty name="user" property="email"/></span>
                                        </li>
                                        <li>
                                            <i class="fa fa-phone mr5"></i>
                                            <span  data-toggle="tooltip" title="Click to set phone number!">
                                                <span id="PhoneSpan" href="#editPhoneDiv" data-toggle="collapse" style="cursor:pointer;" ><jsp:getProperty name="user" property="phoneNumber"/>
                                                </span>
                                            </span>
                                            <!--    <a href="#editPhoneDiv" class="fa fa-edit" data-toggle="collapse"></a> -->
                                            <div id="editPhoneDiv" class="inline collapse inline">
                                                <div class="panel-body">
                                                    <form id="editPhoneForm" role="form" class="col-sm-12 col-md-8 col-lg-6" onSubmit="onSave(event, 'Phone');"> 
                                                        <div class="form-group"> 
                                                            <small><label for="editPhoneInput">Edit phone number</label></small> 
                                                            <input name="value" id="editPhoneInput" class="form-control" placeholder="+39...">
                                                            <span id='editPhoneMessage' class="inline"></span>
                                                            <input type="hidden" name="action" value="editprofile">
                                                            <input type="hidden" name="what" value="phone">
                                                            <button type="submit" class="btn btn-success btn-outline mt5">Save</button>
                                                            <button type="button" class="btn btn-danger btn-outline mt5" onclick="onDiscard(event, 'Phone');">Discard</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </li>

                                        <li>
                                            <i class="fa fa-paypal mr5"></i>
                                            <span  data-toggle="tooltip" title="Click to set paypal!">
                                                <span id="PaypalSpan" href="#editPaypalDiv" data-toggle="collapse" style="cursor:pointer;"><jsp:getProperty name="user" property="paypal"/>
                                                </span>
                                            </span>
                                            <!--           <a href="#editPaypalDiv" class="fa fa-edit" data-toggle="collapse"></a> -->
                                            <div id="editPaypalDiv" class="inline collapse">
                                                <div class="panel-body">
                                                    <form id="editPaypalForm" role="form" class="col-sm-12 col-md-8 col-lg-6" onSubmit="onSave(event, 'Paypal');"> 
                                                        <div class="form-group"> 
                                                            <small><label for="editPaypalInput">Edit Paypal email address</label></small> 
                                                            <input name="value" type="email" class="form-control" id="editPaypalInput" placeholder="Enter mail">
                                                            <span id='editPaypalMessage'></span>
                                                            <input type="hidden" name="action" value="editprofile">
                                                            <input type="hidden" name="what" value="paypal">
                                                            <button type="submit" class="btn btn-success btn-outline mt5 btn-parsley">Save</button>
                                                            <button type="button" class="btn btn-danger btn-outline mt5" onclick="onDiscard(event, 'Paypal');">Discard</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>

                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 text-center">
                                    <figure>
                                        <img src="<jsp:getProperty name="user" property="IMG"/>" alt="" class="avatar avatar-lg img-circle avatar-bordered">
                                    </figure>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 mt25 text-center bt">
                        <h6>Account completation</h6>
                        <div class="progress progress-xs mt5 mb5">
                            <div class="progress-bar" role="progressbar" aria-valuenow="4" aria-valuemin="0" aria-valuemax="6" style="width: 40%">
                            </div>
                        </div>
                    </div>
                </div>




                <div class="row mb15">
                    <div class="col-xs-12">
                        <h6 class="heading-font">About <jsp:getProperty name="user" property="name"/> <jsp:getProperty name="user" property="surname"/>; </h6>
                        <p>Complete your account info, to get a wonderful experience with Pickitup!</p>
                    </div>

                    <div class="col-xs-12 mt15">
                        <h6 class="heading-font">Social Profiles</h6>
                        <div class="mt10 mb10">
                            <a class="btn btn-social btn-xs btn-facebook mr5"><i class="fa fa-facebook"></i>Facebook </a>
                            <a class="btn btn-social btn-xs btn-twitter mr5"><i class="fa fa-twitter"></i>Twitter </a>
                            <a class="btn btn-social btn-xs btn-github mr5"><i class="fa fa-github"></i>Github </a>
                        </div>
                    </div>

                </div>

                <a href="javascript:;" class="text-muted">
                    <i class="fa fa-globe mr15"></i>www.garystone.co</a>
            </div>

            <!-- /profile information sidebar -->
        </div>
    </div>
</div>
<!-- /inner content wrapper -->

</div>
<!-- /content wrapper -->
<a class="exit-offscreen"></a>

<!-- build:js({.tmp,app}) scripts/app.min.js -->
<script src="${pageContext.request.contextPath}/admin/vendor/jquery/dist/jquery.js"></script>
<script src="${pageContext.request.contextPath}/admin/vendor/bootstrap/dist/js/bootstrap.js"></script>
<script src="${pageContext.request.contextPath}/admin/vendor/slimScroll/jquery.slimscroll.js"></script>
<script src="${pageContext.request.contextPath}/admin/vendor/jquery.easing/jquery.easing.js"></script>
<script src="${pageContext.request.contextPath}/admin/vendor/jquery_appear/jquery.appear.js"></script>
<script src="${pageContext.request.contextPath}/admin/vendor/jquery.placeholder.js"></script>
<script src="${pageContext.request.contextPath}/admin/vendor/fastclick/lib/fastclick.js"></script>
<!-- endbuild -->

<!-- page level scripts -->
<!-- /page level scripts -->

<!-- template scripts -->
<script src="${pageContext.request.contextPath}/admin/scripts/offscreen.js"></script>
<script src="${pageContext.request.contextPath}/admin/scripts/main.js"></script>
<!-- /template scripts -->

<!-- page script -->
<script>
                                                                $("[data-toggle=tooltip]").tooltip();
</script>
<!-- /page script -->
<%@include file="../WEB-INF/index-structure-post.html" %>