/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function onSave(e, what) {
    e.preventDefault();
    var formID = 'edit' + what + 'Form';
    var inputText = $('#edit' + what + 'Input');
    var divID = 'edit' + what + 'Div';
    var data = inputText.val();
    var valid = false;
    if (what == 'Phone')
        valid = validatePhone(data);
    else
        valid = validateMail(data);
    if (valid) {
        $.ajax({
            type: "POST",
            url: "ProfileServlet",
            data: $('#' + formID).serialize(), // serializes the form's elements.
            cache: false,
            complete: function (response) {
                if (response.responseText != 'failure') {
                    $('#' + what + 'Span').html(response.responseText);
                    //alert(response.responseText);
                } else if (response.responseText == 'failure') {
                    alert('Something went wrong :(');
                }
                var inputTextID = 'edit' + what + 'Input';
                var divID = 'edit' + what + 'Div';
                $('#' + inputTextID).val('');
                $('#' + divID).collapse('hide');
            }
        });
    } else {
        //con l'autocheck di html5 qui ci arrivo solo tramite telefono non valido
        //per arrivarci anche per via di email non valida cambiare type="text"
        alert('invalid');
        $('#' + divID).addClass('has-error has-feedback');
    }
}

function onDiscard(event, what) {
    event.preventDefault();
    var inputTextID = 'edit' + what + 'Input';
    var divID = 'edit' + what + 'Div';
    $('#' + inputTextID).val('');
    $('#' + divID).collapse('hide');
}

function fakeSubmitEvent(e, what) {
    var formID = 'edit' + what + 'Form';
    ('#' + formID).submit();
}
//non funge
function setErrorLook(what) {
    var input = $('#' + what + 'Input');
    if (!input.hasClass('parsleyError'))
        input.addClass('parsleyError');
}
//non funge
function removeErrorLook(what) {
    var input = $('#' + what + 'Input');
    if (input.hasClass('parsleyError'))
        input.removeClass('parsleyError');
}

function validateMail(mail)
{
    var regex = /^[a-zA-Z0-9]+(([\.|_]?([a-zA-Z0-9]+)))*@[a-zA-Z]+([\.|_]?([a-zA-Z0-9]+))*.[a-zA-Z]{2,3}$/;
    return regex.test(mail);
}

function validatePhone(telefono)
{
    var regex = /^\+?[0-9]{4,14}$/;
    return regex.test(telefono);
}