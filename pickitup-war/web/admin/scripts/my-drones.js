/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//$(function () {$('[data-toggle="tooltip"]').tooltip();})
$('#blah').hide();

function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
}
/**
$(document).ready(function () {
    if($('#cannotDeleteDiv').is(":visible"))
        $('#cannotDeleteDiv').slideToggle('slow');
if(getUrlParameter("message")=='error')
    $('#cannotDeleteDiv').slideToggle('slow');});*/

function updateModal(id, name, flatFee, maxPayload, maxVolume, perKmFee, speed, weight, autonomy, radius, imgURL) {
    $('#myModalLabelId').text('Drone ID: ' + id);
    $('#hiddenDroneId').val(id);
    $('#myModalInputName').val(name);
    $('#myModalInputFlatFee').val(flatFee);
    $('#myModalLabelMaxPayload').text(maxPayload);
    $('#myModalLabelMaxVolume').text(maxVolume);
    $('#myModalInputPerKmFee').val(perKmFee);
    $('#myModalLabelSpeed').text(speed);
    $('#myModalLabelWeight').text(weight);
    $('#myModalLabelAutonomy').text(autonomy);
    $('#myModalLabelRadius').text(radius);
    
    childrenList = $('#myModalImgDivRow').children();
    if(childrenList.length>0)
        $('#droneIMG').remove();
    $('<img id="droneIMG"  src="'+ imgURL +'" class=\"bordered col-sm-3 col-md-6 col-lg-6 no-p ml10" style="max-width:95%; max-height:400px; width: auto; height: auto;">'
            ).load(function() {
        $(this).appendTo('#myModalImgDivRow');
    });
    
}

function toggleOutMessage() {
    if($('#cannotDeleteDiv').is(":visible"))
        $('#cannotDeleteDiv').slideToggle('slow');
}

function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah')
                    .attr('class',"bordered col-sm-3 col-md-6 col-lg-6 no-p ml10")
                    .attr('style',"max-width:95%; max-height:400px; width: auto; height: auto;")
                    .attr('src', e.target.result);
                if(!$('#blah').is(":visible"))
                    $('#blah').slideToggle('slow');
            };

            reader.readAsDataURL(input.files[0]);
        }
}

function clearModalAddDrone() {
    $('#imagePicker').val('');
    if($('#blah').is(":visible"))
        $('#blah').slideToggle('slow');
    $('#myModalInputNameC').val('');
    $('#myModalInputFlatFeeC').val('');
    $('#myModalInputMaxPayload').val('');
    $('#myModalInputMaxVolume').val('');
    $('#myModalInputPerKmFeeC').val('');
    $('#myModalInputSpeed').val('');
    $('#myModalInputWeight').val('');
    $('#myModalInputAutonomy').val('');
    $('#myModalInputRadius').val('');
} 

function deleteTry() {
    var id = $('#hiddenAction').val('delete');
    $('#myModalDroneForm').submit();
}
