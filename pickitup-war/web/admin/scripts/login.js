/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function onSignIn(googleUser) {
    var id_token = googleUser.getAuthResponse().id_token;
    document.getElementById("idtoken").value = id_token;
    document.getElementById("action").value = "login-g";
    document.getElementById("hidden-form").submit();
}


function onFailure(error) {
    console.log("Google Login failed :(" + error);
}

function renderButton() {
    gapi.signin2.render('g-signin2', {
        'scope': 'profile email',
        'width': 'auto',
        'height': '30',
        'longtitle': true,
        'theme': 'dark',
        'onsuccess': onSignIn,
        'onfailure': onFailure
    });
}


function signOut() {
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {

    });
    auth2.disconnect();
}

window.fbAsyncInit = function () {
    FB.init({
        appId: '130242504179104',
        cookie: true, // enable cookies to allow the server to access 
        // the session
        xfbml: true, // parse social plugins on this page
        version: 'v2.8' // use graph api version 2.8
    });

    // Now that we've initialized the JavaScript SDK, we call 
    // FB.getLoginStatus().  This function gets the state of the
    // person visiting this page and can return one of three states to
    // the callback you provide.  They can be:
    //
    // 1. Logged into your app ('connected')
    // 2. Logged into Facebook, but not your app ('not_authorized')
    // 3. Not logged into Facebook and can't tell if they are logged into
    //    your app or not.
    //
    // These three cases are handled in the callback function.


};

// Load the SDK asynchronously
(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id))
        return;
    js = d.createElement(s);
    js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

// Here we run a very simple test of the Graph API after login is
// successful.  See statusChangeCallback() for when this call is made.
function testAPI() {
    console.log('Welcome!  Fetching your information.... ');
    FB.api('/me', {fields: 'name, email, picture'}, function (response) {
        console.log('Good to see you, ' + response.name + '.' + ' Email: ' + response.email + ' Facebook ID: ' + response.id);
    });
}

function doLoginFB() {
    FB.login(function (response) {
        if (response.status === 'connected') {
            console.log(response.authResponse.accessToken);
            FB.api('/me', {fields: 'name, email, picture'}, function (response) {
                console.log(JSON.stringify(response));
                console.log('Good to see you, ' + response.name + '.' + ' Email: ' + response.email + ' Facebook ID: ' + response.id);
                /* 
                 document.getElementById("name").value = response.name;
                 document.getElementById("imgurl").value = "http://graph.facebook.com/" + response.id + "/picture?type=normal";
                 document.getElementById("email").value = response.email;
                 */

            });
            document.getElementById("idtoken").value = response.authResponse.accessToken;
            document.getElementById("action").value = "login-f";
            document.getElementById("hidden-form").submit();
        } else {
            //non sei riuscito a connetterti
        }
    });
}


function logoutGoogle() {
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
        window.location.href = 'SignupServlet?action=logout';
    });
    auth2.disconnect();
}

function logoutFacebook() {
    FB.init({
        appId: '130242504179104', cookie: true,
        status: true, xfbml: true
    });
    FB.getLoginStatus(function(response) {
        if (response && response.status === 'connected') {
            FB.logout(function () {
                window.location.href = 'SignupServlet?action=logout';
            });
        } else {
             window.location.href = 'SignupServlet?action=logout';
        }
    });
}

function genericLogout(typeLogin) {
    if (typeLogin == "google") {
        logoutGoogle();
    } else {
        logoutFacebook();
    }
}