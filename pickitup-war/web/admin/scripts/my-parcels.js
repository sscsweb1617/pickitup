$(document).ready(function () {
    if ($('#modalOpened').val() != '') {
        $('#modalNewParcel').modal('show');
        $('#name').val($('#nameParcelFromHome').val());
    }
});



function updateModal(id, name, estimatedValue, volume, weight) {
    $('#parcelId').text('Parcel ' + id);
    $('#parcelName').text(name);
    $('#parcelValue').text(estimatedValue + ' \u20AC');
    $('#parcelVolume').text(volume + ' cm');
    $("#parcelVolume").append("<sup>3</sup>");
    $('#parcelWeight').text(weight + ' kg');
    
    $('#buttonDelete').removeAttr("disabled");
    $('#tooltipParcel').removeAttr("data-toggle");
    $('#tooltipParcel').removeAttr("title");

    $.ajax({
        url: "ParcelServlet",
        type: "POST",
        data: {action: "controlStatusParcel", idParcel: id},
        success: function (data) {
            if (data == "true") {
                $('#idParcel').val(id)
            } else {
                $('#buttonDelete').attr('disabled', true);
                $('#tooltipParcel').attr('data-toggle', "tooltip");

                $('#tooltipParcel').attr('title', "Probably this parcel has a delivery!");
                $('body').tooltip({
                    selector: '[data-toggle=tooltip]'
                });
            }
        }
    });
}

