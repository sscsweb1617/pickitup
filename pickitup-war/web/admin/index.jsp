<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:useBean id="user" class="model.entity.AppUser" scope="session" />

<jsp:include page="../WEB-INF/index-structure-pre.jsp"/>
<!-- content wrapper -->
<div class="content-wrap">

    <!-- inner content wrapper -->
    <div class="wrapper">

        <div class="row">
            <div class="col-md-12 col-lg-8" >

                <section class="panel panel-main" style="overflow: hidden;">
                    <header class="panel-heading">
                        <span> <b> Hey! It looks like it's full of drones around you... Browse the map! </b></span>
                    </header>

                    <footer class="panel-footer text-center" style="padding-left: 0px; padding-right: 0px; padding-top: 0px; padding-bottom: 0px;border-top-width: 0px;">
                        <div class="row">
                            <div id="map" class="col-lg-8 col-md-10 col-sm-12 col-xs-12" style="min-height: 370px;"></div>
                            <script>
                                function initMap() {
                                    if (navigator.geolocation) {
                                        navigator.geolocation.getCurrentPosition(riempiMappa, function() {alert("Can't find your position!")}
                                        ,{timeout:10000});
                                    }
                                }
                                function riempiMappa(geoloc) {
                                    var pos = new google.maps.LatLng({
                                        lat: geoloc.coords.latitude,
                                        lng: geoloc.coords.longitude
                                    });

                                    var map = new google.maps.Map(document.getElementById('map'), {
                                        zoom: 6,
                                        center: pos
                                    });
                                    new google.maps.Marker({
                                        position: pos,
                                        map: map,
                                        icon: '${pageContext.request.contextPath}/admin/images/user_marker.png'
                                    });
                                }
                            </script>
                            <script async defer
                                    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDeIE_WpzAcCUD4v0QH6J2W2T417ZB6UYA&callback=initMap">
                            </script>
                        </div>
                    </footer>
                </section>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12" >

                <section class="widget">
                    <div class="widget-header bg-info">
                        <i class="fa fa-newspaper-o text-white"></i>
                        <b>Newsfeed</b>
                    </div>
                    <section class="panel"> 
                        <div class="row"> 
                            <div class="col-md-12"> <div class="carousel slide" data-ride="carousel" id="quote-carousel" style="min-height: 150px;"> 
                                    <ol class="carousel-indicators">
                                        <li data-target="#quote-carousel" data-slide-to="0" class="active"></li>
                                        <li data-target="#quote-carousel" data-slide-to="1" class=""></li>
                                        <li data-target="#quote-carousel" data-slide-to="2" class=""></li>
                                    </ol> 
                                    <div class="carousel-inner"> 
                                        <div class="item next left">
                                            <div class="row"> 
                                                <div class="col-sm-3 text-center"> 
                                                    <img class="img-circle avatar avatar-md" src="http://lorempixel.com/75/75/people/" alt=""> 
                                                </div>
                                                <div class="col-sm-9"> 
                                                    <p>Next iteration will provide the web application the function "Call a drone". You won't need to bring your parcel anywhere!</p>
                                                    <small> <i>Our team</i> </small>
                                                </div>
                                            </div>
                                        </div> 
                                        <div class="item"> 
                                            <div class="row"> 
                                                <div class="col-sm-3 text-center"> 
                                                    <img class="img-circle avatar avatar-md" src="http://lorempixel.com/75/75/people/" alt=""> 
                                                </div> <div class="col-sm-9"> 
                                                    <p>What in the future? Drone's cooperation, no more courier vans blocking traffic all around!</p> 
                                                    <small> <i>Our team</i> </small> </div>
                                            </div> </div> <div class="item active left"> 
                                            <div class="row"> 
                                                <div class="col-sm-3 text-center"> 
                                                    <img class="img-circle avatar avatar-md" src="http://lorempixel.com/75/75/people/" alt=""> 
                                                </div> 
                                                <div class="col-sm-9"> 
                                                    <p>PickItUp will turn out to be the best horse to bet on! Contact us to invest in our business.</p> 
                                                    <small> <i>CEO</i> </small> 
                                                </div>
                                            </div> 
                                        </div> 
                                    </div> 
                                    <a data-slide="prev" href="#quote-carousel" class="left carousel-control"> <i class="ti-arrow-circle-left"></i> </a> 
                                    <a data-slide="next" href="#quote-carousel" class="right carousel-control"> <i class="ti-arrow-circle-right"></i> </a>
                                </div>
                            </div>
                        </div>
                    </section>
                </section>

            </div>
            <c:if test="${user.class.simpleName == 'Client'}">
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <section class="panel panel-success" style="overflow: hidden;">
                        <header class="panel-heading">
                            <div class="h5">
                                <i class="fa fa-send mr5"></i>
                                <b>Send your parcel</b>
                            </div>
                        </header>
                        <footer class="panel-footer" style="padding-left: 0px; padding-right: 0px; padding-top: 0px; padding-bottom: 0px;">
                            <div class="row">
                                <div class="col-lg-12" style="padding-left: 0px; padding-right: 0px; padding-top: 0px; padding-bottom: 0px;">
                                    <section class="panel">
                                        <header class="panel-heading"><b>What is it?</b></header>
                                        <div class="panel-body"> 
                                            <form action="${pageContext.request.contextPath}/ParcelServlet?action=parcelFromHome" method="POST" class="form-inline" role="form">
                                                <div class="form-group mr5"> 
                                                    <input class="form-control" name="nameParcel" placeholder="Parcel name">
                                                </div>
                                                <button type="submit" class="btn btn-rounded">Lets go!</button> 
                                            </form> 
                                        </div> 
                                    </section> 
                                </div>  
                            </div>
                        </footer>
                    </section>
                </div>
            </c:if>
        </div> 
        <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                <section class="widget">
                    <div class="widget-header bg-info">
                        <i class="ti-arrow-circle-up text-white"></i>
                        <span class="h3">4.48</span>
                        <span>k</span>
                        <span class="small show text-uppercase">+23% User Growth</span>
                    </div>
                    <div class="widget-body bg-info">
                        <div class="text-center mt15 mb10">
                            <span class="dash-line"></span>
                        </div>
                    </div>
                    <footer class="widget-footer text-center">
                        <div class="row">
                            <div class="col-xs-4">
                                <div class="small show text-uppercase text-muted">Deliveries</div>
                                <div class="h4 no-m"><b>5467</b>
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div class="small show text-uppercase text-muted">Feedback</div>
                                <div class="h4 no-m"><b>6873</b>
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div class="small show text-uppercase text-muted">Drones</div>
                                <div class="h4 no-m"><b>8</b>
                                </div>
                            </div>
                        </div>
                    </footer>
                </section>
            </div>

            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                <section class="widget panel panel-warning">
                    <header class="panel-heading">
                        <div class="h5">
                            <i class="fa fa-info-circle mr5"></i>
                            <b>Perspective</b>
                        </div>
                    </header>
                    <footer class="panel-footer text-center">
                        <div class="row">
                            <div class="col-xs-4">
                                <div class="small show text-uppercase text-muted">Flying boxes</div>
                                <div class="h4 no-m"><b>5434</b>
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div class="small show text-uppercase text-muted">Parcels sent</div>
                                <div class="h4 no-m"><b>894</b>
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div class="small show text-uppercase text-muted">Parcels shipped</div>
                                <div class="h4 no-m"><b>08</b>
                                </div>
                            </div>
                        </div>
                    </footer>
                </section>
            </div>


        </div>
    </div>

</div>
<!-- /inner content wrapper -->

<!-- /content wrapper -->
<a class="exit-offscreen"></a>


<!-- build:js({.tmp,app}) scripts/app.min.js -->
<script src="${pageContext.request.contextPath}/admin/vendor/jquery/dist/jquery.js"></script>
<script src="${pageContext.request.contextPath}/admin/vendor/bootstrap/dist/js/bootstrap.js"></script>
<script src="${pageContext.request.contextPath}/admin/vendor/slimScroll/jquery.slimscroll.js"></script>
<script src="${pageContext.request.contextPath}/admin/vendor/jquery.easing/jquery.easing.js"></script>
<script src="${pageContext.request.contextPath}/admin/vendor/jquery_appear/jquery.appear.js"></script>
<script src="${pageContext.request.contextPath}/admin/vendor/jquery.placeholder.js"></script>
<script src="${pageContext.request.contextPath}/admin/vendor/fastclick/lib/fastclick.js"></script>
<!-- endbuild -->

<!-- page level scripts -->
<script src="${pageContext.request.contextPath}/admin/vendor/blockUI/jquery.blockUI.js"></script>
<script src="${pageContext.request.contextPath}/admin/vendor/bower-jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="${pageContext.request.contextPath}/admin/data/maps/jquery-jvectormap-world-mill-en.js"></script>
<script src="${pageContext.request.contextPath}/admin/vendor/jquery.sparkline.js"></script>
<script src="${pageContext.request.contextPath}/admin/vendor/flot/jquery.flot.js"></script>
<script src="${pageContext.request.contextPath}/admin/vendor/flot/jquery.flot.resize.js"></script>
<script src="${pageContext.request.contextPath}/admin/vendor/jquery-countTo/jquery.countTo.js"></script>
<!-- /page level scripts -->

<!-- page script -->
<script src="${pageContext.request.contextPath}/admin/scripts/dashboard.js"></script>
<!-- /page script -->

<!-- template scripts -->
<script src="${pageContext.request.contextPath}/admin/scripts/offscreen.js"></script>
<script src="${pageContext.request.contextPath}/admin/scripts/main.js"></script>
<!-- /template scripts -->

<script>
                                $(function () {
                                    if (navigator.geolocation) {
                                        navigator.geolocation.getCurrentPosition(setPosition);
                                    }
                                });
                                function setPosition(pos) {
                                    $.post('${pageContext.request.contextPath}/ProfileServlet?action=setPosition', {
                                        latitude: pos.coords.latitude,
                                        longitude: pos.coords.longitude
                                    });
                                }
</script>

<%@include file="../WEB-INF/index-structure-post.html" %>