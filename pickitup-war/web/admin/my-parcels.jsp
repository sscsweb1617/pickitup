<%@page import="model.entity.Parcel"%>
<%@page import="model.entity.Client"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="../WEB-INF/index-structure-pre.jsp"/>
<jsp:useBean id="parcel" class="model.entity.Parcel" scope="request" />
<jsp:useBean id="user" class="model.entity.Client" scope="session" />
<script src="${pageContext.request.contextPath}/admin/scripts/my-parcels.js"></script>
<script>
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>

<!-- content wrapper -->
<div class="content-wrap">



    <!-- inner content wrapper -->
    <div class="wrapper">       
        <div class="panel-body">                
            <c:if test="${user.parcelList.size() == 0}">
                <h4>You don't have any parcel yet.</h4>
            </c:if>                  
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="row">
                    <button type="button" class="btn btn-primary btn-outline" data-toggle="modal" data-target="#modalNewParcel">Create new parcel</button>
                </div>
            </div>
        </div>


        <input type="hidden" id="modalOpened" value="${requestScope.modalOpened}">
        <input type="hidden" id="nameParcelFromHome" value="${requestScope.nameParcelFromHome}">

        <div class="modal fade" id="modalNewParcel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Create new parcel</h4>
                    </div>
                    <form action="${pageContext.request.contextPath}/ParcelServlet?action=new" method="POST" class="form-horizontal" role="form">

                        <div style="padding: 15px">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Name</label>
                                        <div class="col-sm-9">
                                            <input class="form-control" id="name" name="name" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Estimated value (&#8364)</label>
                                        <div class="col-sm-9">
                                            <input type="number" class="form-control" name="estimatedValue" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Volume (cm<sup>3</sup>)</label>
                                        <div class="col-sm-9">
                                            <input type="number" class="form-control" name="volume" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Weight (Kg)</label>
                                        <div class="col-sm-9">
                                            <input type="number" min="0" step="0.1" class="form-control" name="weight" required>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <input type="submit" value="Create" id="buttonCreate" class="btn btn-primary">
                        </div>
                    </form>      
                </div>
            </div>
        </div>


        <!-- Modal open parcel-->
        <div class="modal fade" id="openParcel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="parcelId">Parcel xyz</h4>
                    </div>
                    <div style="padding: 15px">
                        <div class="row">
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="panel panel-primary"> 
                                    <header class="panel-heading"> 
                                        <div class="h5">  
                                            <b>Name</b> 
                                        </div> 
                                    </header> 
                                    <footer class="panel-footer text-center"> 
                                        <div class="row"> 
                                            <div class="col-xs-12"> 
                                                <div class="p no-m"><b id="parcelName">xyz</b> </div> 
                                            </div> 
                                        </div>
                                    </footer> 
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="panel panel-primary"> 
                                    <header class="panel-heading"> 
                                        <div class="h5">  
                                            <b>Est. Value</b> 
                                        </div> 
                                    </header> 
                                    <footer class="panel-footer text-center"> 
                                        <div class="row"> 
                                            <div class="col-xs-12"> 
                                                <div class="p no-m"><b id="parcelValue">xyz</b> </div> 
                                            </div> 
                                        </div>
                                    </footer> 
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="panel panel-primary"> 
                                    <header class="panel-heading"> 
                                        <div class="h5">  
                                            <b>Volume</b> 
                                        </div> 
                                    </header> 
                                    <footer class="panel-footer text-center"> 
                                        <div class="row"> 
                                            <div class="col-xs-12"> 
                                                <div class="p no-m"><b id="parcelVolume">xyz</b></div> 
                                            </div> 
                                        </div>
                                    </footer> 
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="panel panel-primary"> 
                                    <header class="panel-heading"> 
                                        <div class="h5">  
                                            <b>Weight</b> 
                                        </div> 
                                    </header> 
                                    <footer class="panel-footer text-center"> 
                                        <div class="row"> 
                                            <div class="col-xs-12"> 
                                                <div class="p no-m"><b id="parcelWeight">xyz</b> </div> 
                                            </div> 
                                        </div>
                                    </footer> 
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <form action="${pageContext.request.contextPath}/ParcelServlet?action=deleteParcel" method="POST">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <input type="hidden" id="idParcel" name="idParcel" value="">
                            <span id="tooltipParcel">
                                <button type="submit" id="buttonDelete" class="btn btn-danger">Delete parcel</button>
                            </span>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">

            <div class="panel-body">

                <c:forEach items="${user.parcelList}" var="parcel">
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4" style="cursor:pointer"
                         data-toggle="modal"
                         data-target="#openParcel"
                         onclick="updateModal('${parcel.id}', '${parcel.name}', '${parcel.estimatedValue}',
                                         '${parcel.volume}', '${parcel.weight}')"> 
                        <section class="widget"> 
                            <div class="widget-header bg-info"> 
                                <span class="h3">Parcel name: ${parcel.name}</span> 
                                <!--<span class="small show text-uppercase">Description:...</span> -->
                            </div> 
                            <div class="widget-body bg-info"> 
                                <div class="text-center mt15 mb10"> 
                                    <span class="dash-line"><canvas width="310" height="40" style="display: inline-block; width: 310px; height: 40px; vertical-align: top;">

                                        </canvas>
                                    </span> 
                                </div> 
                            </div> 
                            <footer class="widget-footer text-center"> 
                                <div class="row"> 
                                    <div class="col-xs-4"> 
                                        <div class="small show text-uppercase text-muted">E. Value (&#8364)</div> 
                                        <div class="h4 no-m"><b>${parcel.estimatedValue}</b> </div> 
                                    </div> <div class="col-xs-4"> 
                                        <div class="small show text-uppercase text-muted">Volume (cm<sup>3</sup>)</div> 
                                        <div class="h4 no-m"><b>${parcel.volume}</b> </div> 
                                    </div> <div class="col-xs-4"> 
                                        <div class="small show text-uppercase text-muted">Weight (kg)</div> 
                                        <div class="h4 no-m"><b>${parcel.weight}</b> 
                                        </div> </div> </div> 
                            </footer> 
                        </section> 
                    </div>
                </c:forEach>
            </div>


        </div>
    </div>


</div>
<!-- /content wrapper -->
<a class="exit-offscreen"></a>

<!-- build:js({.tmp,app}) scripts/app.min.js -->
<script src="${pageContext.request.contextPath}/admin/vendor/jquery/dist/jquery.js"></script>
<script src="${pageContext.request.contextPath}/admin/vendor/bootstrap/dist/js/bootstrap.js"></script>
<script src="${pageContext.request.contextPath}/admin/vendor/slimScroll/jquery.slimscroll.js"></script>
<script src="${pageContext.request.contextPath}/admin/vendor/jquery.easing/jquery.easing.js"></script>
<script src="${pageContext.request.contextPath}/admin/vendor/jquery_appear/jquery.appear.js"></script>
<script src="${pageContext.request.contextPath}/admin/vendor/jquery.placeholder.js"></script>
<script src="${pageContext.request.contextPath}/admin/vendor/fastclick/lib/fastclick.js"></script>
<!-- endbuild -->

<!-- page level scripts -->
<!-- /page level scripts -->

<!-- template scripts -->
<script src="${pageContext.request.contextPath}/admin/scripts/offscreen.js"></script>
<script src="${pageContext.request.contextPath}/admin/scripts/main.js"></script>
<!-- /template scripts -->

<!-- page script -->
<script>
                             $("[data-toggle=tooltip]").tooltip();
</script>
<!-- /page script -->
<%@include file="../WEB-INF/index-structure-post.html" %>