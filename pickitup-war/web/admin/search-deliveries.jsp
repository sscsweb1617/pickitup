<%@page import="model.entity.Delivery"%>
<%@page import="model.entity.Client"%>
<%@page import="model.entity.Parcel"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="../WEB-INF/index-structure-pre.jsp"/>

<jsp:useBean id="deliveries" class="java.util.AbstractList" scope="request" />
<jsp:useBean id="parcels" class="java.util.AbstractList" scope="request" />
<jsp:useBean id="user" class="model.entity.Client" scope="session" />

<!-- content wrapper -->
<div class="content-wrap">
    <!-- inner content wrapper -->
    <div class="wrapper">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel">
                    <header class="panel-heading">Delivery research</header>
                    <div class="panel-body">
                        <form action="${pageContext.request.contextPath}/DeliveryServlet?action=search" method="post" class="form-horizontal">

                            <div class="form-group">
                                <label class="col-sm-1 control-label">Parcel</label>

                                <div class="col-sm-11">

                                    <select class="form-control" name="parcel" id="parcel" required>
                                        <option disabled selected value> -- select a parcel -- </option>
                                        <c:forEach items="${parcels}" var="p">
                                            <option value="${p.id}">${p.name}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </div>

                            <!-- Google Map interface -->
                            <div class="form-group">
                                <label class="col-sm-12">Click on your parcel's destination point:</label>
                                <div class="col-sm-12">
                                    <input id="destLat" type="hidden" name="destLat">
                                    <input id="destLng" type="hidden" name="destLng">
                                    <div id="searchMap" style="height: 20em"></div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input id="search-btn" type="submit" class="btn btn-block btn-default" disabled value="Search Deliveries"/>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <c:if test="${searchResult}">
            <c:if test="${deliveries.size() == 0}">
                <div class="panel-body">
                    <h4>Sorry, no delivery found!</h4>
                    <p>
                        Maybe there isn't any drone that could manage your parcel in that zone.<br>
                        Try to change your research parameters.
                    </p>
                </div>
            </c:if>

            <c:if test="${deliveries.size() > 0}">
                <div id="search-ok" class="row">
                    <div class="col-md-12">
                        <div class="panel">
                            <div class="panel-heading no-b">
                                <h5><b>Choose your delivery</b></h5>
                            </div>
                            <div class="panel-body">
                                <table class="table no-m table-hover">
                                    <thead>
                                        <tr>
                                            <th class="col-md-1">ID</th>
                                            <th class="col-md-5">Drone</th>
                                            <th class="col-md-3">Est. Time (minutes)</th>
                                            <th class="col-md-3">Cost (Euros)</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach items="${deliveries}" var="delivery">
                                            <tr style="cursor:pointer"
                                                data-toggle="modal"
                                                data-target="#1"
                                                onclick="updateModal(${delivery.id}, '${delivery.drone}', '${delivery.drone.owner}',
                                                ${delivery.estimatedTime}, ${Math.round(100 * delivery.cost) / 100.0},
                                                ${delivery.drone.owner.position.latitude}, ${delivery.drone.owner.position.longitude})">
                                                <td>${delivery.id}</td>
                                                <td>${delivery.drone}</td>
                                                <td>${delivery.estimatedTime}</td>
                                                <td>${Math.round(100 * delivery.cost) / 100.0}</td>
                                            </tr>
                                        </c:forEach>


                                    </tbody>
                                </table>
                            </div>

                            <!-- Modal -->
                            <div class="modal fade" id="1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Delivery xyz</h4>
                                        </div>
                                        <div style="padding: 15px">
                                            <div class="row">
                                                <div id="modalMap" style="height: 20em"></div>
                                            </div>
                                            <hr>
                                            <div class="row">
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <div class="panel panel-primary"> 
                                                        <header class="panel-heading"> 
                                                            <div class="h5">  
                                                                <b>Drone</b> 
                                                            </div> 
                                                        </header> 
                                                        <footer class="panel-footer text-center"> 
                                                            <div class="row"> 
                                                                <div class="col-xs-12"> 
                                                                    <div class="p no-m"><b id="droneName">xyz</b> </div> 
                                                                </div> 
                                                            </div>
                                                        </footer> 
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <div class="panel panel-primary"> 
                                                        <header class="panel-heading"> 
                                                            <div class="h5">  
                                                                <b>Owner</b> 
                                                            </div> 
                                                        </header> 
                                                        <footer class="panel-footer text-center"> 
                                                            <div class="row"> 
                                                                <div class="col-xs-12"> 
                                                                    <div class="p no-m"><b id="ownerName">xyz</b> </div> 
                                                                </div> 
                                                            </div>
                                                        </footer> 
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <div class="panel panel-primary"> 
                                                        <header class="panel-heading"> 
                                                            <div class="h5">  
                                                                <b>Time est.</b> 
                                                            </div> 
                                                        </header> 
                                                        <footer class="panel-footer text-center"> 
                                                            <div class="row"> 
                                                                <div class="col-xs-12"> 
                                                                    <div class="p no-m"><b id="timeVal">xyz minutes</b> </div> 
                                                                </div> 
                                                            </div>
                                                        </footer> 
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <div class="panel panel-primary"> 
                                                        <header class="panel-heading"> 
                                                            <div class="h5">  
                                                                <b>Cost</b> 
                                                            </div> 
                                                        </header> 
                                                        <footer class="panel-footer text-center"> 
                                                            <div class="row"> 
                                                                <div class="col-xs-12"> 
                                                                    <div class="p no-m"><b id="costVal">xyz &euro;</b> </div> 
                                                                </div> 
                                                            </div>
                                                        </footer> 
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                                            <!-- Bottone per pagare via PayPal -->
                                            <script src="https://www.paypalobjects.com/api/checkout.js"></script>
                                            <div id="paypal-button" class="btn">Pay and start the delivery!</div>
                                            <script>
                                                    paypal.Button.render({
                                                        locale: 'en_US',
                                                        style: {
                                                            size: 'medium',
                                                            color: 'blue',
                                                            shape: 'rect',
                                                            label: 'checkout'
                                                        },
                                                        
                                                        env: 'sandbox',
                                                        client: {
                                                            sandbox: 'AQIVFhnFOGzNbvIVxWoZVrCy_GhAXTlZHz5fbj5SxDdQI7-2AuKeLIhQHWh-E_W0VHpHzEK-tT1uYrAq',
                                                            production: 'undefined'
                                                        },
                                                        commit: true,

                                                        payment: function (data, actions) {
                                                            return actions.payment.create({
                                                                transactions: [
                                                                    {
                                                                        amount: {total: paypalAmount, currency: 'EUR'}
                                                                    }
                                                                ]
                                                            });
                                                        },
                                                        onAuthorize: function (data, actions) {
                                                            return actions.payment.execute().then(function (payment) {
                                                                location.href = '${pageContext.request.contextPath}/DeliveryServlet?action=book&id=' + paypalDeliveryId;
                                                            });
                                                        }

                                                    }, '#paypal-button');
                                            </script>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="row">
                                <a href="${pageContext.request.contextPath}/DeliveryServlet?action=search" class="btn btn-primary btn-outline">
                                    Clear results
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </c:if>
        </c:if>
    </div>
    <!-- /inner content wrapper -->

</div>
<!-- /content wrapper -->
<a class="exit-offscreen"></a>

<!-- build:js({.tmp,app}) scripts/app.min.js -->
<script src="${pageContext.request.contextPath}/admin/vendor/jquery/dist/jquery.js"></script>
<script src="${pageContext.request.contextPath}/admin/vendor/bootstrap/dist/js/bootstrap.js"></script>
<script src="${pageContext.request.contextPath}/admin/vendor/slimScroll/jquery.slimscroll.js"></script>
<script src="${pageContext.request.contextPath}/admin/vendor/jquery.easing/jquery.easing.js"></script>
<script src="${pageContext.request.contextPath}/admin/vendor/jquery_appear/jquery.appear.js"></script>
<script src="${pageContext.request.contextPath}/admin/vendor/jquery.placeholder.js"></script>
<script src="${pageContext.request.contextPath}/admin/vendor/fastclick/lib/fastclick.js"></script>
<!-- endbuild -->

<!-- page level scripts -->
<!-- /page level scripts -->

<!-- template scripts -->
<script src="${pageContext.request.contextPath}/admin/scripts/offscreen.js"></script>
<script src="${pageContext.request.contextPath}/admin/scripts/main.js"></script>
<!-- /template scripts -->

<!-- page script -->
<script type="text/javascript">
                                                    var searchMap;
                                                    var userMarker;
                                                    var destinationMarker;
                                                    var pathLine;
                                                    var modalMap;
                                                    var modalUserMarker;
                                                    var modalDestMarker;
                                                    var modalDroneMarker;
                                                    var modalPathLine;
                                                    function initMap() {
                                                        searchMap = new google.maps.Map(document.getElementById('searchMap'),
                                                                {
                                                                    center: {
                                                                        lat: ${user.position.latitude},
                                                                        lng: ${user.position.longitude}
                                                                    },
                                                                    zoom: 12
                                                                });
                                                        userMarker = new google.maps.Marker({
                                                            position: {
                                                                lat: ${user.position.latitude},
                                                                lng: ${user.position.longitude}
                                                            },
                                                            map: searchMap,
                                                            icon: '${pageContext.request.contextPath}/admin/images/user_marker.png'
                                                        });
                                                        if (document.getElementById('search-ok') === null) {
                                                            google.maps.event.addDomListener(searchMap, 'click', onMapClick);
                                                        } else {
                                                            onMapClick({
                                                                latLng: new google.maps.LatLng({
                                                                    lat: ${pageContext.request.getParameter("destLat")}0,
                                                                    lng: ${pageContext.request.getParameter("destLng")}0
                                                                })
                                                            });
                                                            document.getElementById('parcel').value = "${pageContext.request.getParameter("parcel")}";
                                                            modalMap = new google.maps.Map(document.getElementById('modalMap'),
                                                                    {
                                                                        center: {
                                                                            lat: ${user.position.latitude},
                                                                            lng: ${user.position.longitude}
                                                                        },
                                                                        zoom: 12
                                                                    });
                                                            modalDroneMarker = new google.maps.Marker({
                                                                position: null,
                                                                map: modalMap,
                                                                icon: '${pageContext.request.contextPath}/admin/images/drone_marker.png'
                                                            });
                                                            modalUserMarker = new google.maps.Marker({
                                                                position: {
                                                                    lat: ${user.position.latitude},
                                                                    lng: ${user.position.longitude}
                                                                },
                                                                map: modalMap,
                                                                icon: '${pageContext.request.contextPath}/admin/images/user_marker.png'
                                                            });
                                                            modalDestMarker = new google.maps.Marker({
                                                                position: {
                                                                    lat: ${pageContext.request.getParameter("destLat")}0,
                                                                    lng: ${pageContext.request.getParameter("destLng")}0
                                                                },
                                                                map: modalMap,
                                                                icon: '${pageContext.request.contextPath}/admin/images/dest_marker.png'
                                                            });
                                                            modalPathLine = new google.maps.Polyline({
                                                                path: [],
                                                                map: modalMap
                                                            });
                                                            $('#1').on('shown.bs.modal', function () {
                                                                google.maps.event.trigger(modalMap, "resize");
                                                            });
                                                        }
                                                    }

                                                    function onMapClick(event) {
                                                        if (destinationMarker === undefined) {
                                                            destinationMarker = new google.maps.Marker({
                                                                position: event.latLng,
                                                                map: searchMap,
                                                                icon: '${pageContext.request.contextPath}/admin/images/dest_marker.png'
                                                            });
                                                            pathLine = new google.maps.Polyline({
                                                                path: [userMarker.position, destinationMarker.position],
                                                                map: searchMap,
                                                                weight: 2,
                                                                color: '#0077ff'
                                                            });
                                                            pathLine.setMap(searchMap);
                                                            document.getElementById("search-btn").disabled = false;
                                                        } else {
                                                            destinationMarker.setPosition(event.latLng);
                                                            pathLine.getPath().setAt(1, event.latLng);
                                                        }
                                                        document.getElementById("destLat").value = event.latLng.lat();
                                                        document.getElementById("destLng").value = event.latLng.lng();
                                                    }

                                                    function updateModal(id, drone, owner, time, cost, acceptanceLat, acceptanceLng) {
                                                        $('#myModalLabel').text('Delivery ' + id);
                                                        $('#droneName').text(drone);
                                                        $('#ownerName').text(owner);
                                                        $('#timeVal').text(time + ' minutes');
                                                        $('#costVal').text(cost + ' \u20AC');
                                                        $('#start-delivery').attr('href', 'javascript:alert(\'Noooo\')');
                                                        modalDroneMarker.setPosition({
                                                            lat: acceptanceLat,
                                                            lng: acceptanceLng
                                                        });
                                                        modalPathLine.setPath([
                                                            modalUserMarker.position,
                                                            modalDroneMarker.position,
                                                            modalDestMarker.position
                                                        ]);
                                                        paypalAmount = cost;
                                                        paypalDeliveryId = id;
                                                    }
</script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBOyK_EmMO5tb4ScFHGYs69J5l53NI73cI&callback=initMap" async defer></script>
<!-- /page scripts -->

<%@include file="../WEB-INF/index-structure-post.html" %>