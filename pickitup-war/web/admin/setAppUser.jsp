<!doctype html>
<html class="signin no-js" lang="">
    <head>
        <!-- meta -->
        <meta charset="utf-8">
        <meta name="description" content="Flat, Clean, Responsive, application admin template built with bootstrap 3">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
        <!-- /meta -->

        <title>Pickitup!</title>

        <!-- page level plugin styles -->
        <!-- /page level plugin styles -->

        <!-- build:css({.tmp,app}) styles/app.min.css -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/admin/vendor/bootstrap/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/admin/styles/font-awesome.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/admin/styles/themify-icons.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/admin/styles/animate.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/admin/styles/sublime.css">
        <!-- endbuild -->


        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
              <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
              <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
          <![endif]-->

        <!-- load modernizer -->
        <script src="vendor/modernizr.js"></script>
        <script src="scripts/login.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
    </head>

    <body class="bg-primary">

        <div class="cover" style="background-image: url(${pageContext.request.contextPath}/admin/images/drone2.jpg)"></div>

        <div class="overlay bg-primary"></div>

        <div class="center-wrapper">
            <div class="center-content">


                <div class="row no-m">
                    <div class="col-lg-offset-1 col-md-offset-2 col-sm-offset-1">
                        <div class="col-xs-10 col-xs-offset-1 col-sm-5 col-sm-offset-3 col-md-4 col-md-offset-3 col-lg-3 col-lg-offset-4">
                            <section class="panel bg-white no-b">
                                <ul class="list-group">
                                    <li class="list-group-item" style="border: none">
                                        <h4 class="text-center text-primary">
                                            <em>Who do you want to be in</em>
                                            <strong>
                                                PickitUp?
                                            </strong>
                                        </h4>
                                    </li>
                                </ul>
                                <ul class="list-group">
                                    <li class="list-group-item" style="border: none">
                                        <form class="text-center" action="${pageContext.request.contextPath}/SignupServlet" method="post">
                                            <ul class="list-inline" style="margin-left: -25px;">
                                                <li style="padding-right: 30px; padding-left: 15px;"><label class="radio-inline">
                                                        <input type="radio" name="optradio" value="DroneOwner"><strong>
                                                            Drone Owner
                                                        </strong>
                                                    </label>
                                                </li>
                                                <li><label class="radio-inline bold">
                                                        <input type="radio" name="optradio" value="Client"><strong>
                                                            Client
                                                        </strong>
                                                    </label>
                                                </li>
                                            </ul>
                                            <ul class="list-group" style="margin-bottom: 0px; margin-top: 10px;">
                                                <li class="list-group-item" style="border: none;">
                                                    <button type="submit" name="action" value="setAppUser" type="button" class="btn btn-primary btn-sm btn-icon loading-demo mr5" data-loading-text="Sending data ..."> 
                                                        <i class="ti-share mr5"></i> 
                                                        <span>Let's start!</span>                              
                                                    </button>
                                                </li>
                                            </ul>
                                        </form>
                                    </li>
                                </ul>                  
                            </section>
                            <p class="text-center">
                                Copyright &copy;
                                <span id="year" class="mr5"></span>
                                <span>Sublime LLC</span>
                            </p>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
</div>
<script type="text/javascript">
    var el = document.getElementById("year"),
            year = (new Date().getFullYear());
    el.innerHTML = year;
</script>
</body>
</html>
