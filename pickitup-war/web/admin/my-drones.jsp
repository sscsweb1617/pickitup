<%@page import="model.entity.Drone"%>
<%@page import="model.entity.DroneOwner"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../WEB-INF/index-structure-pre.jsp"/>
<jsp:useBean id="drones" class="java.util.AbstractList" scope="request" />
<jsp:useBean id="user" class="model.entity.DroneOwner" scope="session" />
<%final String message = (String) request.getAttribute("message");%>
<script src="${pageContext.request.contextPath}/admin/scripts/my-drones.js"></script>

<!-- content wrapper -->
<div class="content-wrap">

    <!-- inner content wrapper -->
    <div class="wrapper">
        
        <c:if test="${drones.size() == 0}">
                <div class="panel-body">
                    <h4>You don't have any drone yet.</h4>
                    <c:if test="${user.class.simpleName == 'DroneOwner'}">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="row">
                                <a data-toggle="modal" data-target="#modalCreateDrone" class="btn btn-primary btn-outline">
                                    Add a drone now!
                                </a>
                            </div>
                        </div>
                    </c:if>
                </div>
            </c:if>

        <c:if test="${drones.size() > 0}">
                <div class="row">
                    <div class="col-md-12">
                        <section class="panel">
                            <div class="panel-heading no-b">
                                <h5><b>My drones</b></h5>
                            </div>
                            <div class="panel-body">
                                <table class="table no-m table-hover">
                                    <thead>
                                        <tr>
                                            <th class="col-md-1 text-center hidden-md hidden-sm hidden-xs">ID</th>
                                            <th class="col-md-2 text-center">Name</th>
                                            <th class="col-md-3 text-center">Flat fee <br> (&euro;)</th>
                                            <th class="col-md-2 text-center">Fee <br> (&euro;/Km)</th>
                                            <th class="col-md-2 text-center">Max payload <br> (Kg)</th>
                                            <th class="col-md-2 text-center">Max volume <br> (cm<sup>3</sup>)</th>
                                            <th class="col-md-2 text-center hidden-xs">Speed <br> (Km/h)</th>
                                            <th class="col-md-2 text-center hidden-sm  hidden-xs">Weight <br> (Kg)</th>
                                            <th class="col-md-2 text-center hidden-xs">Autonomy <br> (min)</th>
                                            <th class="col-md-2 text-center">Coverage <br> radius (m)</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach items="${drones}" var="drone">
                                            <tr style="cursor:pointer" data-toggle="modal" data-target="#modalDrone"
                                                onclick="updateModal(${drone.id},'${drone.name}',${drone.flatFee},${drone.maxPayload},
                                                            ${drone.maxVolume},${drone.perKmFee},${drone.speed},${drone.weight},
                                                            ${drone.autonomy},${drone.coverage.radius},'${pageContext.request.contextPath}/${drone.image}')">
                                                <td class="text-center hidden-md hidden-sm hidden-xs">${drone.id}</td>
                                                <td class="text-center">${drone.name}</td>
                                                <td class="text-center">${drone.flatFee}</td>
                                                <td class="text-center">${drone.perKmFee}</td>
                                                <td class="text-center">${drone.maxPayload}</td>
                                                <td class="text-center">${drone.maxVolume}</td>
                                                <td class="text-center hidden-xs">${drone.speed}</td>
                                                <td class="text-center hidden-sm hidden-xs">${drone.weight}</td>
                                                <td class="text-center hidden-xs">${drone.autonomy}</td>
                                                <td class="text-center">${drone.coverage.radius}</td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                                <c:if test="${message != null}">
                                    <div id='cannotDeleteDiv' class='row'>
                                        <div class="col-lg-offset-3 col-lg-6 col-md-offset-2 col-md-8 col-sm-12 text-center">
                                            <div class='alert alert-warning alert-dismissable text-center'>
                                                <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
                                                <strong>Warning!</strong>&nbsp; Sorry, can't delete a drone busy in a delivery.
                                            </div>
                                        </div>
                                    </div>
                                </c:if>
                            </div>
                            <!-- Modal -->
                            <div class="modal fade" id="modalDrone" onclose="toggleOutMessage()" tabindex="-1" role="dialog" aria-labelledby="myModalLabelId">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                    <form action='${pageContext.request.contextPath}/DroneServlet' method='post' id='myModalDroneForm'
                                          class="form-horizontal" role="form">
                                        <input id="hiddenDroneId" type="hidden" name="droneId">
                                        <input id="hiddenAction" type="hidden" name="action" value='edit'>
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabelId">Drone ID:</h4>
                                        </div>
                                        <div class="p10">
                                            <div class="row">
                                                <div>
                                                <div id="myModalImgDivRow" class="col-xs-12 col-sm-6 col-md-6 col-lg-6 text-center" style="padding:0px;"></div>
                                                </div>
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <section class="panel panel-primary" style="padding-bottom: 8px;"> 
                                                        <header class="panel-heading text-center"> 
                                                            <div class="h5">  
                                                                <b>Name</b> 
                                                            </div> 
                                                        </header> 
                                                        <footer class="panel-footer text-center" style="padding-top: 17px;"> 
                                                            <div class="row"> 
                                                                <div class="col-xs-12"> 
                                                                    <div class="h4 no-m"><input id="myModalInputName" class="form-control" type="text" id="nameInput" name="name" required></div> 
                                                                </div> 
                                                        </footer> 
                                                    </section>
                                                </div>
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <section class="panel panel-primary"> 
                                                        <header class="panel-heading text-center"> 
                                                            <div class="h5">  
                                                                <b>Flat fee <br> (&euro;)</b> 
                                                            </div> 
                                                        </header> 
                                                        <footer class="panel-footer text-center"> 
                                                            <div class="row"> 
                                                                <div class="col-xs-12"> 
                                                                    <div class="h4 no-m">
                                                                        <input id="myModalInputPerKmFee" class="form-control" type="number" min="0" step="0.05" id="flatFeeInput" name="flatFee" required></div> 
                                                                </div> 
                                                        </footer> 
                                                    </section>
                                                </div>
                                                <div class="col-md-3 col-sm-6 col-xs-12">  
                                                    <section class="panel panel-primary"> 
                                                        <header class="panel-heading text-center"> 
                                                            <div class="h5">  
                                                                <b>Fee <br> (&euro;/Km)</b> 
                                                            </div> 
                                                        </header> 
                                                        <footer class="panel-footer text-center"> 
                                                            <div class="row"> 
                                                                <div class="col-xs-12">
                                                                    <div class="h4 no-m"><input id="myModalInputFlatFee" class="form-control" min="0" step="0.05" type="number" id="perKmFeeInput" name="perKmFee" required></div> 
                                                                </div> 
                                                        </footer> 
                                                    </section>
                                                </div>
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <section class="panel panel-primary pb10"> 
                                                        <header class="panel-heading text-center" style="padding-bottom: 5px;"> 
                                                            <div class="h5">  
                                                                <b>Speed <br> (Km/h)</b> 
                                                            </div> 
                                                        </header> 
                                                        <footer class="panel-footer text-center" style="padding-top: 20px;"> 
                                                            <div class="row"> 
                                                                <div class="col-xs-12"> 
                                                                    <div class="h4 no-m"><b id="myModalLabelSpeed"></b> </div> 
                                                                </div> 
                                                        </footer> 
                                                    </section>
                                                </div>
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <section class="panel panel-primary"> 
                                                        <header class="panel-heading text-center"> 
                                                            <div class="h5">  
                                                                <b>Weight <br> (Kg)</b> 
                                                            </div> 
                                                        </header> 
                                                        <footer class="panel-footer text-center"> 
                                                            <div class="row"> 
                                                                <div class="col-xs-12"> 
                                                                    <div class="h4 no-m"><b id="myModalLabelWeight"></b> </div> 
                                                                </div> 
                                                        </footer> 
                                                    </section>
                                                </div>
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <section class="panel panel-primary"> 
                                                        <header class="panel-heading text-center"> 
                                                            <div class="h5">  
                                                                <b>Autonomy <br> (min)</b> 
                                                            </div> 
                                                        </header> 
                                                        <footer class="panel-footer text-center">
                                                            <div class="row"> 
                                                                <div class="col-xs-12"> 
                                                                    <div class="h4 no-m"><b id="myModalLabelAutonomy"></b> </div> 
                                                                </div> 
                                                        </footer> 
                                                    </section>
                                                </div><div class="col-md-3 col-sm-6 col-xs-12">
                                                    <section class="panel panel-primary"> 
                                                        <header class="panel-heading text-center"> 
                                                            <div class="h5">  
                                                                <b>Max payload <br> (Kg)</b> 
                                                            </div> 
                                                        </header> 
                                                        <footer class="panel-footer text-center"> 
                                                            <div class="row"> 
                                                                <div class="col-xs-12"> 
                                                                    <div class="h4 no-m"><b id="myModalLabelMaxPayload"></b> </div> 
                                                                </div> 
                                                        </footer> 
                                                    </section>
                                                </div><div class="col-md-3 col-sm-6 col-xs-12">
                                                    <section class="panel panel-primary"> 
                                                        <header class="panel-heading text-center"> 
                                                            <div class="h5">  
                                                                <b>Max volume <br> (cm<sup>3</sup>)</b> 
                                                            </div> 
                                                        </header> 
                                                        <footer class="panel-footer text-center"> 
                                                            <div class="row"> 
                                                                <div class="col-xs-12"> 
                                                                    <div class="h4 no-m"><b id="myModalLabelMaxVolume"></b> </div> 
                                                                </div> 
                                                        </footer> 
                                                    </section>
                                                </div><div class="col-md-3 col-sm-6 col-xs-12">
                                                    <section class="panel panel-primary"> 
                                                        <header class="panel-heading text-center"> 
                                                            <div class="h5">  
                                                                <b>Coverage radius <br> (m)</b> 
                                                            </div> 
                                                        </header> 
                                                        <footer class="panel-footer text-center"> 
                                                            <div class="row"> 
                                                                <div class="col-xs-12"> 
                                                                    <div class="h4 no-m"><b id="myModalLabelRadius"></b> </div> 
                                                                </div> 
                                                        </footer> 
                                                    </section>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Save changes</button>
                                            <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="deleteTry()"><i class="fa fa-trash"></i></button>
                                        </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                    </section>
                    
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="row">
                        <a data-toggle="modal" data-target="#modalCreateDrone" class="btn btn-primary btn-outline">
                            Add a drone
                        </a>
                    </div>
                </div>
            </div>
        </c:if>
        <!-- Modal drone creation-->
                            <div class="modal fade" id="modalCreateDrone" tabindex="-1" role="dialog" aria-labelledby="myModalCreateId">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                    <form action='${pageContext.request.contextPath}/DroneServlet?' method='post' id='myModalAddDroneForm'
                                          class="form-horizontal" enctype="multipart/form-data" role="form">
                                        <input type="hidden" name="action" value="add">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabelId">Add a new drone to your fleet</h4>
                                        </div>
                                        <div style="padding: 15px">
                                            <div class="row">
                                                <div class="panel">
                                                    <label class="ml5" for="imagePicker">Image</label><br>
                                                    <div id="imageLoader" class="text-center">
                                                        <img id="blah" src="#" alt="your image" style="display:none;"/>
                                                        <input name="file" id="imagePicker" type='file' class="pt15 mb15 ml5" onchange="readURL(this);" accept="image/*" required/>
                                                   </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <section class="panel panel-primary" style="padding-bottom: 8px;"> 
                                                        <header class="panel-heading text-center"> 
                                                            <div class="h5">  
                                                                <b>Name</b> 
                                                            </div> 
                                                        </header> 
                                                        <footer class="panel-footer text-center" style="padding-top: 17px;"> 
                                                            <div class="row"> 
                                                                <div class="col-xs-12"> 
                                                                    <div class="h4 no-m"><input id="myModalInputNameC" class="form-control" type="text" id="nameInput" name="name" required></div> 
                                                                </div> 
                                                        </footer> 
                                                    </section>
                                                </div>
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <section class="panel panel-primary"> 
                                                        <header class="panel-heading text-center"> 
                                                            <div class="h5">  
                                                                <b>Flat fee <br> (&euro;)</b> 
                                                            </div> 
                                                        </header> 
                                                        <footer class="panel-footer text-center"> 
                                                            <div class="row"> 
                                                                <div class="col-xs-12"> 
                                                                    <div class="h4 no-m">
                                                                        <input id="myModalInputPerKmFeeC" class="form-control" type="number" min="0" step="0.05" name="flatFee" required></div> 
                                                                </div> 
                                                        </footer> 
                                                    </section>
                                                </div>
                                                <div class="col-md-3 col-sm-6 col-xs-12">  
                                                    <section class="panel panel-primary"> 
                                                        <header class="panel-heading text-center"> 
                                                            <div class="h5">  
                                                                <b>Fee <br> (&euro;/Km)</b> 
                                                            </div> 
                                                        </header> 
                                                        <footer class="panel-footer text-center"> 
                                                            <div class="row"> 
                                                                <div class="col-xs-12">
                                                                    <div class="h4 no-m"><input id="myModalInputFlatFeeC" class="form-control" min="0" step="0.05" type="number" name="perKmFee" required></div> 
                                                                </div> 
                                                        </footer> 
                                                    </section>
                                                </div>
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <section class="panel panel-primary"> 
                                                        <header class="panel-heading text-center"> 
                                                            <div class="h5">  
                                                                <b>Speed <br> (Km/h)</b> 
                                                            </div> 
                                                        </header> 
                                                        <footer class="panel-footer text-center"> 
                                                            <div class="row"> 
                                                                <div class="col-xs-12"> 
                                                                    <div class="h4 no-m"><input type="number" class="form-control" min="0" step="0.5" id="myModalInputSpeed" name="speed" required></div> 
                                                                </div> 
                                                        </footer> 
                                                    </section>
                                                </div>
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <section class="panel panel-primary"> 
                                                        <header class="panel-heading text-center"> 
                                                            <div class="h5">  
                                                                <b>Weight <br> (Kg)</b> 
                                                            </div> 
                                                        </header> 
                                                        <footer class="panel-footer text-center"> 
                                                            <div class="row"> 
                                                                <div class="col-xs-12"> 
                                                                    <div class="h4 no-m"><input type="number" class="form-control" min="0" step="0.1" id="myModalInputWeight" name="weight" required></div> 
                                                                </div> 
                                                        </footer> 
                                                    </section>
                                                </div>
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <section class="panel panel-primary"> 
                                                        <header class="panel-heading text-center"> 
                                                            <div class="h5">  
                                                                <b>Autonomy <br> (min)</b> 
                                                            </div> 
                                                        </header> 
                                                        <footer class="panel-footer text-center">
                                                            <div class="row"> 
                                                                <div class="col-xs-12"> 
                                                                    <div class="h4 no-m"><input type="number" class="form-control" min="0" step="1" id="myModalInputAutonomy" name="autonomy" required></div> 
                                                                </div> 
                                                        </footer> 
                                                    </section>
                                                </div><div class="col-md-3 col-sm-6 col-xs-12">
                                                    <section class="panel panel-primary"> 
                                                        <header class="panel-heading text-center"> 
                                                            <div class="h5">  
                                                                <b>Max payload <br> (Kg)</b> 
                                                            </div> 
                                                        </header> 
                                                        <footer class="panel-footer text-center"> 
                                                            <div class="row"> 
                                                                <div class="col-xs-12"> 
                                                                    <div class="h4 no-m"><input type="number" class="form-control" min="0" step="0.05" id="myModalInputMaxPayload" name="maxPayload" required></div> 
                                                                </div> 
                                                        </footer> 
                                                    </section>
                                                </div><div class="col-md-3 col-sm-6 col-xs-12">
                                                    <section class="panel panel-primary"> 
                                                        <header class="panel-heading text-center"> 
                                                            <div class="h5">  
                                                                <b>Max volume <br> (cm<sup>3</sup>)</b> 
                                                            </div> 
                                                        </header> 
                                                        <footer class="panel-footer text-center"> 
                                                            <div class="row"> 
                                                                <div class="col-xs-12"> 
                                                                    <div class="h4 no-m"><input type="number" class="form-control" min="0" step="1" id="myModalInputMaxVolume" name="maxVolume" required></div> 
                                                                </div> 
                                                        </footer> 
                                                    </section>
                                                </div><div class="col-md-3 col-sm-6 col-xs-12">
                                                    <section class="panel panel-primary"> 
                                                        <header class="panel-heading text-center"> 
                                                            <div class="h5">  
                                                                <b>Coverage radius <br> (m)</b> 
                                                            </div> 
                                                        </header> 
                                                        <footer class="panel-footer text-center"> 
                                                            <div class="row"> 
                                                                <div class="col-xs-12"> 
                                                                    <div class="h4 no-m"><input type="number" class="form-control" min="0" step="0.5" id="myModalInputRadius" name="coverage" required></div> 
                                                                </div> 
                                                        </footer> 
                                                    </section>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-primary">Add drone</button>
                                            <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="clearModalAddDrone()">Discard</button>
                                        </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
    </div>
    <!-- /inner content wrapper -->

</div>
<!-- /content wrapper -->
<a class="exit-offscreen"></a>

<!-- build:js({.tmp,app}) scripts/app.min.js -->
<script src="${pageContext.request.contextPath}/admin/vendor/jquery/dist/jquery.js"></script>
<script src="${pageContext.request.contextPath}/admin/vendor/bootstrap/dist/js/bootstrap.js"></script>
<script src="${pageContext.request.contextPath}/admin/vendor/slimScroll/jquery.slimscroll.js"></script>
<script src="${pageContext.request.contextPath}/admin/vendor/jquery.easing/jquery.easing.js"></script>
<script src="${pageContext.request.contextPath}/admin/vendor/jquery_appear/jquery.appear.js"></script>
<script src="${pageContext.request.contextPath}/admin/vendor/jquery.placeholder.js"></script>
<script src="${pageContext.request.contextPath}/admin/vendor/fastclick/lib/fastclick.js"></script>
<!-- page level scripts -->
<script src="${pageContext.request.contextPath}/admin/vendor/isotope/dist/isotope.pkgd.min.js"></script>
<!-- /page level scripts -->

<!-- template scripts -->
<script src="${pageContext.request.contextPath}/admin/scripts/offscreen.js"></script>
<script src="${pageContext.request.contextPath}/admin/scripts/main.js"></script>
<!-- /template scripts -->

<!-- page script -->
<script src="${pageContext.request.contextPath}/admin/scripts/catalog.js"></script>
<!-- /page script -->

<%@include file="../WEB-INF/index-structure-post.html" %>