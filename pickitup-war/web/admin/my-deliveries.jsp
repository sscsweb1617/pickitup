<%@page import="model.entity.Delivery"%>
<%@page import="model.entity.Client"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="../WEB-INF/index-structure-pre.jsp"/>
<jsp:useBean id="deliveries" class="java.util.AbstractList" scope="request" />
<jsp:useBean id="user" class="model.entity.AppUser" scope="session" />

<!-- content wrapper -->
<div class="content-wrap">

    <!-- inner content wrapper -->
    <div class="content-wrap">

        <!-- inner content wrapper -->
        <div class="wrapper">
            <c:if test="${deliveries.size() == 0}">
                <div class="panel-body">
                    <h4>You don't have any delivery yet.</h4>
                    <c:if test="${user.class.simpleName == 'Client'}">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="row">
                                <a href="${pageContext.request.contextPath}/DeliveryServlet?action=search" class="btn btn-primary btn-outline">
                                    Send a parcel now!
                                </a>
                            </div>
                        </div>
                    </c:if>
                </div>
            </c:if>

            <c:if test="${deliveries.size() > 0}">
                <div class="row">
                    <div class="col-md-12">
                        <section class="panel">
                            <div class="panel-heading no-b">
                                <h5><b>My deliveries</b></h5>
                            </div>
                            <div class="panel-body">
                                <table class="table no-m table-hover">
                                    <thead>
                                        <tr>
                                            <th class="col-md-1">ID</th>
                                            <th class="col-md-3">Parcel</th>
                                            <th class="col-md-2">Destination</th>
                                            <th class="col-md-2">Drone</th>
                                            <th class="col-md-1">Est. Time (minutes)</th>
                                            <th class="col-md-1">Cost (&euro;)</th>
                                            <th class="col-md-2">Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach items="${deliveries}" var="delivery">
                                            <tr style="cursor:pointer"
                                                data-toggle="modal"
                                                data-target="#1"
                                                onclick="updateModal('${user.class.simpleName}', ${delivery.id},
                                                                '${delivery.status}', '${delivery.status.description}',
                                                                '${delivery.drone}', '${delivery.drone.owner}',
                                                ${delivery.estimatedTime}, ${Math.round(100 * delivery.cost) / 100.0},
                                                ${delivery.client.position.latitude}, ${delivery.client.position.longitude},
                                                ${delivery.drone.owner.position.latitude}, ${delivery.drone.owner.position.longitude},
                                                ${delivery.destination.latitude}, ${delivery.destination.longitude})">
                                                <td>${delivery.id}</td>
                                                <td>${delivery.parcel}</td>
                                                <td>${delivery.destination}</td>
                                                <td>${delivery.drone}</td>
                                                <td>${delivery.estimatedTime}</td>
                                                <td>${Math.round(100 * delivery.cost) / 100.0}</td>
                                                <td>
                                                    <div class="progress"
                                                         data-toggle="tooltip"
                                                         data-placement="top"
                                                         title="${delivery.status.toString()}">
                                                        <div class="progress-bar"
                                                             role="progressbar"
                                                             aria-valuenow="${delivery.status.value}"
                                                             aria-valuemin="0"
                                                             aria-valuemax="100"
                                                             style="width: ${delivery.status.value}%;">
                                                            <span class="sr-only">${delivery.status.toString()}</span>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                            </div>

                            <!-- Modal -->
                            <div class="modal fade" id="1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Delivery xyz</h4>
                                            <p id="statusDescription" />
                                        </div>
                                        <div style="padding: 15px">
                                            <div class="row">
                                                <div id="modalMap" style="height: 20em"></div>
                                            </div>
                                            <hr>
                                            <div class="row">
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <div class="panel panel-primary"> 
                                                        <header class="panel-heading"> 
                                                            <div class="h5">  
                                                                <b>Drone</b> 
                                                            </div> 
                                                        </header> 
                                                        <footer class="panel-footer text-center"> 
                                                            <div class="row"> 
                                                                <div class="col-xs-12"> 
                                                                    <div class="p no-m"><b id="droneName">xyz</b> </div> 
                                                                </div> 
                                                            </div>
                                                        </footer> 
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <div class="panel panel-primary"> 
                                                        <header class="panel-heading"> 
                                                            <div class="h5">  
                                                                <b>Owner</b> 
                                                            </div> 
                                                        </header> 
                                                        <footer class="panel-footer text-center"> 
                                                            <div class="row"> 
                                                                <div class="col-xs-12"> 
                                                                    <div class="p no-m"><b id="ownerName">xyz</b> </div> 
                                                                </div> 
                                                            </div>
                                                        </footer> 
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <div class="panel panel-primary"> 
                                                        <header class="panel-heading"> 
                                                            <div class="h5">  
                                                                <b>Time est.</b> 
                                                            </div> 
                                                        </header> 
                                                        <footer class="panel-footer text-center"> 
                                                            <div class="row"> 
                                                                <div class="col-xs-12"> 
                                                                    <div class="p no-m"><b id="timeVal">xyz minutes</b> </div> 
                                                                </div> 
                                                            </div>
                                                        </footer> 
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <div class="panel panel-primary"> 
                                                        <header class="panel-heading"> 
                                                            <div class="h5">  
                                                                <b>Cost</b> 
                                                            </div> 
                                                        </header> 
                                                        <footer class="panel-footer text-center"> 
                                                            <div class="row"> 
                                                                <div class="col-xs-12"> 
                                                                    <div class="p no-m"><b id="costVal">xyz &euro;</b> </div> 
                                                                </div> 
                                                            </div>
                                                        </footer> 
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <a id="actionBtn" class="btn btn-primary">xyz</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </c:if>
        </div>
    </div>
    <!-- /inner content wrapper -->

</div>
<!-- /content wrapper -->
<a class="exit-offscreen"></a>

<!-- build:js({.tmp,app}) scripts/app.min.js -->
<script src="${pageContext.request.contextPath}/admin/vendor/jquery/dist/jquery.js"></script>
<script src="${pageContext.request.contextPath}/admin/vendor/bootstrap/dist/js/bootstrap.js"></script>
<script src="${pageContext.request.contextPath}/admin/vendor/slimScroll/jquery.slimscroll.js"></script>
<script src="${pageContext.request.contextPath}/admin/vendor/jquery.easing/jquery.easing.js"></script>
<script src="${pageContext.request.contextPath}/admin/vendor/jquery_appear/jquery.appear.js"></script>
<script src="${pageContext.request.contextPath}/admin/vendor/jquery.placeholder.js"></script>
<script src="${pageContext.request.contextPath}/admin/vendor/fastclick/lib/fastclick.js"></script>
<!-- endbuild -->

<!-- page level scripts -->
<!-- /page level scripts -->

<!-- template scripts -->
<script src="${pageContext.request.contextPath}/admin/scripts/offscreen.js"></script>
<script src="${pageContext.request.contextPath}/admin/scripts/main.js"></script>
<!-- /template scripts -->

<!-- page script -->
<script>
                                                    $(function () {
                                                        $('[data-toggle="tooltip"]').tooltip();
                                                    })

                                                    var modalMap;
                                                    var modalClientMarker;
                                                    var modalDestMarker;
                                                    var modalDroneMarker;
                                                    var modalPathLine;
                                                    function initMap() {
                                                        modalMap = new google.maps.Map(document.getElementById('modalMap'));
                                                        modalDroneMarker = new google.maps.Marker({
                                                            position: null,
                                                            map: modalMap,
                                                            icon: '${pageContext.request.contextPath}/admin/images/drone_marker.png'
                                                        });
                                                        modalClientMarker = new google.maps.Marker({
                                                            position: null,
                                                            map: modalMap,
                                                            icon: '${pageContext.request.contextPath}/admin/images/user_marker.png'
                                                        });
                                                        modalDestMarker = new google.maps.Marker({
                                                            position: null,
                                                            map: modalMap,
                                                            icon: '${pageContext.request.contextPath}/admin/images/dest_marker.png'
                                                        });
                                                        modalPathLine = new google.maps.Polyline({
                                                            path: [],
                                                            map: modalMap
                                                        });
                                                        $('#1').on('shown.bs.modal', function () {
                                                            google.maps.event.trigger(modalMap, "resize");
                                                        });
                                                    }

                                                    function updateModal(role, id, status, statusDesc,
                                                            drone, owner, time, cost,
                                                            clientLat, clientLng,
                                                            acceptanceLat, acceptanceLng,
                                                            destinationLat, destinationLng) {
                                                        $('#myModalLabel').text('Delivery ' + id);
                                                        $('#statusDescription').text(statusDesc);
                                                        $('#droneName').text(drone);
                                                        $('#ownerName').text(owner);
                                                        $('#timeVal').text(time + ' minutes');
                                                        $('#costVal').text(cost + ' \u20AC');
                                                        switch (status) {
                                                            case 'BOOKED':
                                                                $('#actionBtn').attr('href', '${pageContext.request.contextPath}/DeliveryServlet?action=deliveryAccepted&id=' + id);
                                                                $('#actionBtn').text('Parcel accepted');
                                                                $('#actionBtn').css("display","inline");
                                                                break;
                                                            case 'ACCEPTED':
                                                                if (role === "DroneOwner") {
                                                                    $('#actionBtn').attr('href', '${pageContext.request.contextPath}/DeliveryServlet?action=deliverySent&id=' + id);
                                                                    $('#actionBtn').text('I\'ve just sent the parcel!');
                                                                    $('#actionBtn').css("display","inline");
                                                                } else {
                                                                    $('#actionBtn').css("display","none");
                                                                }
                                                                break;
                                                            case 'SENT':
                                                                if (role === "DroneOwner") {
                                                                    $('#actionBtn').attr('href', '${pageContext.request.contextPath}/DeliveryServlet?action=deliveryDone&id=' + id);
                                                                    $('#actionBtn').text('The parcel arrived at destination');
                                                                    $('#actionBtn').css("display","inline");
                                                                } else {
                                                                    $('#actionBtn').css("display","none");
                                                                }
                                                                break;
                                                            default:
                                                                $('#actionBtn').css("display","none");
                                                                break;
                                                        }

                                                        modalDroneMarker.setPosition({
                                                            lat: acceptanceLat,
                                                            lng: acceptanceLng
                                                        });
                                                        modalClientMarker.setPosition({
                                                            lat: clientLat,
                                                            lng: clientLng
                                                        });
                                                        modalDestMarker.setPosition({
                                                            lat: destinationLat,
                                                            lng: destinationLng
                                                        });
                                                        modalPathLine.setPath([
                                                            modalClientMarker.position,
                                                            modalDroneMarker.position,
                                                            modalDestMarker.position
                                                        ]);
                                                        var bounds = new google.maps.LatLngBounds();
                                                        var path = modalPathLine.getPath();
                                                        for (var i = 0; i < path.length; i++) {
                                                            bounds.extend(path.getAt(i));
                                                        }
                                                        modalMap.fitBounds(bounds);
                                                        modalMap.setZoom(12);
                                                    }
</script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBOyK_EmMO5tb4ScFHGYs69J5l53NI73cI&callback=initMap" async defer></script>
<!-- /page script -->
<%@include file="../WEB-INF/index-structure-post.html" %>