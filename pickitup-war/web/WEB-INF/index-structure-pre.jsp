<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:useBean id="user" class="model.entity.AppUser" scope="session"/>
<!doctype html>
<html class="no-js" lang="">

    <head>
        <!-- meta -->
        <meta charset="utf-8">
        <meta name="description" content="Flat, Clean, Responsive, application admin template built with bootstrap 3">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
        <meta name="google-signin-client_id" content="1082698830205-jpfe92smraars628vf7b0ch7od162scp.apps.googleusercontent.com">
        <!-- /meta -->

        <title>Pickitup!</title>

        <!-- page level plugin styles -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/admin/vendor/bower-jvectormap/jquery-jvectormap-1.2.2.css">
        <!-- /page level plugin styles -->

        <!-- build:css({.tmp,app}) styles/app.min.css -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/admin/vendor/bootstrap/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/admin/styles/font-awesome.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/admin/styles/themify-icons.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/admin/styles/animate.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/admin/styles/sublime.css">
        <!-- endbuild -->

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
              <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
              <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
          <![endif]-->

        <!-- load modernizer -->
        <script src="${pageContext.request.contextPath}/admin/vendor/modernizr.js"></script>


        <script src="https://apis.google.com/js/platform.js" async defer></script>
        <script src="http://connect.facebook.net/en_US/all.js"></script>
        <script src="${pageContext.request.contextPath}/admin/scripts/login.js"></script>  
        <script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
    </head>

    <!-- body -->

    <body>

        <div class="app">
            <!-- top header -->
            <header class="header header-fixed navbar">

              <div class="g-signin2"  style="display: none"></div>  

                <div class="brand">
                    <!-- toggle offscreen menu -->
                    <a href="javascript:;" class="ti-menu off-left visible-xs" data-toggle="offscreen" data-move="ltr"></a>
                    <!-- /toggle offscreen menu -->

                    <!-- logo -->
                    <a href="${pageContext.request.contextPath}/admin/index.jsp" class="navbar-brand">
                        <img src="${pageContext.request.contextPath}/admin/images/dronelogo.png" alt="">
                        <span class="heading-font">
                            Pickitup!
                        </span>
                    </a>
                    <!-- /logo -->
                </div>

                <ul class="nav navbar-nav">
                    <li class="hidden-xs">
                        <!-- toggle small menu -->
                        <a href="javascript:;" class="toggle-sidebar">
                            <i class="ti-menu"></i>
                        </a>
                        <!-- /toggle small menu -->
                    </li>
                </ul>

                <ul class="nav navbar-nav navbar-right">

                    <!--                    <li class="notifications dropdown">
                                            <a href="javascript:;" data-toggle="dropdown">
                                                <i class="ti-bell"></i>
                                                <div class="badge badge-top bg-danger animated flash">
                                                    <span>3</span>
                                                </div>
                                            </a>
                                            <div class="dropdown-menu animated fadeInLeft">
                                                <div class="panel panel-default no-m">
                                                    <div class="panel-heading small"><b>Notifications</b>
                                                    </div>
                                                    <ul class="list-group">
                                                        <li class="list-group-item">
                                                            <a href="javascript:;">
                                                                <span class="pull-left mt5 mr15">
                                                                    <img src="images/face4.jpg" class="avatar avatar-sm img-circle" alt="">
                                                                </span>
                                                                <div class="m-body">
                                                                    <div class="">
                                                                        <small><b>CRYSTAL BROWN</b></small>
                                                                        <span class="label label-danger pull-right">ASSIGN AGENT</span>
                                                                    </div>
                                                                    <span>Opened a support query</span>
                                                                    <span class="time small">2 mins ago</span>
                                                                </div>
                                                            </a>
                                                        </li>
                                                        <li class="list-group-item">
                                                            <a href="javascript:;">
                                                                <div class="pull-left mt5 mr15">
                                                                    <div class="circle-icon bg-danger">
                                                                        <i class="ti-download"></i>
                                                                    </div>
                                                                </div>
                                                                <div class="m-body">
                                                                    <span>Upload Progress</span>
                                                                    <div class="progress progress-xs mt5 mb5">
                                                                        <div class="progress-bar" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                                                        </div>
                                                                    </div>
                                                                    <span class="time small">Submited 23 mins ago</span>
                                                                </div>
                                                            </a>
                                                        </li>
                                                        <li class="list-group-item">
                                                            <a href="javascript:;">
                                                                <span class="pull-left mt5 mr15">
                                                                    <img src="images/face3.jpg" class="avatar avatar-sm img-circle" alt="">
                                                                </span>
                                                                <div class="m-body">
                                                                    <em>Status Update:</em>
                                                                    <span>All servers now online</span>
                                                                    <span class="time small">5 days ago</span>
                                                                </div>
                                                            </a>
                                                        </li>
                                                    </ul>
                    
                                                    <div class="panel-footer">
                                                        <a href="javascript:;">See all notifications</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>-->

                    <li class="off-right">
                        <a href="javascript:;" data-toggle="dropdown">
                            <img src="<jsp:getProperty name="user" property="IMG"/>" class="header-avatar img-circle" alt="user" title="user">
                            <span class="hidden-xs ml10"><jsp:getProperty name="user" property="name"/></span>
                            <i class="ti-angle-down ti-caret hidden-xs"></i>
                        </a>
                        <ul class="dropdown-menu animated fadeInRight">
                            <li>
                                <a href="${pageContext.request.contextPath}/UserMenu?action=profile">My Profile</a>
                            </li>
                            <li>
                                <a href="javascript:genericLogout('${sessionScope.typeLogin}')">Logout</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </header>
            <!-- /top header -->

            <div class="layout">
                <!-- sidebar menu -->
                <aside class="sidebar offscreen-left">
                    <!-- main navigation -->
                    <nav class="main-navigation" data-height="auto" data-size="6px" data-distance="0" data-rail-visible="true" data-wheel-step="10">
                        <p class="nav-title">MENU</p>
                        <ul class="nav">
                            <li>
                                <a href="${pageContext.request.contextPath}/UserMenu">
                                    <i class="ti-home"></i>
                                    <span>Home</span>
                                </a>
                            </li>

                            <c:choose>
                                <c:when test="${user.class.simpleName == 'DroneOwner'}">
                                    <li>
                                        <a href="javascript:;">
                                            <i class="toggle-accordion"></i>
                                            <i class="ti-rocket"></i>
                                            <span>Drones</span>
                                        </a>
                                        <ul class="sub-menu">
                                            <li>
                                                <a href="${pageContext.request.contextPath}/DroneServlet">
                                                    <span>My drones</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a 
                                                    href="javascript:alert('Function not yet implemented!\nMaybe on next iteration ;)')">
                                                    <!--href="${pageContext.request.contextPath}/UserMenu?action=in-zone">-->
                                                    <span>In zone</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="${pageContext.request.contextPath}/DeliveryServlet">
                                            <i class="ti-map-alt"></i>
                                            <span>Deliveries</span>
                                        </a>
                                    </li>
                                </c:when>
                                <c:otherwise>
                                    <li>
                                        <a href="${pageContext.request.contextPath}/UserMenu?action=parcels">
                                            <i class="ti-package"></i>
                                            <span>My Parcels</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            <i class="toggle-accordion"></i>
                                            <i class="ti-map-alt"></i>
                                            <span>Deliveries</span>
                                        </a>
                                        <ul class="sub-menu">
                                            <li>
                                                <a href="${pageContext.request.contextPath}/DeliveryServlet?action=search">
                                                    <span>Send a parcel!</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="${pageContext.request.contextPath}/DeliveryServlet">
                                                    <span>My deliveries</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                </c:otherwise>
                            </c:choose>
                            <li>
                                <a href="${pageContext.request.contextPath}/UserMenu?action=profile">
                                    <i class="ti-user"></i>
                                    <span>Profile</span>
                                </a>
                            </li>

                        </ul>
                    </nav>
                </aside>
                <!-- /sidebar menu -->

                <div class="main-content">
                    <!-- main content begins here -->