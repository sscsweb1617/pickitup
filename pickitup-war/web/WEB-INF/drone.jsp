<jsp:useBean id="drone" scope="page" class="model.entity.Drone" />
<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 switch-item accounting">
    <section class="panel">
        <div class="thumb">
            <img class="img-responsive" alt="Responsive image" src="<jsp:getProperty name="drone" property="image"/>"
        </div>
        <div class="panel-body">
            <div class="switcher-content">
                <p>
                    <jsp:getProperty name="drone" property="name"/>
                </p>
                <ul>
                    <li>
                        <i class="fa-dollar"></i>
                        <jsp:getProperty name="drone" property="flatFee" /> + kilometers*<jsp:getProperty name="drone" property="perKmFee"/>
                    </li>
                </ul>
                <a href="javascript:;" class="show small">getbootstrap.com</a>
            </div>
        </div>
    </section>
</div>
